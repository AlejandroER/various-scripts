//+------------------------------------------------------------------+
//|                                                   AllMAFract.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Global and Input Variables                                       |
//+------------------------------------------------------------------+
input int AlligatorBars=1;
input int EMA25Bars=10;
input double EMA25Perc=0.8;
input int BeforeFract=3;
input int AddTP=10;
bool Wait=false;
ulong OrderTicket[];
int NumOrders=0;
int OrderType[];
double LastPrice=0,LastSL=0;
CTrade trade;
datetime LastOpen=StringToTime("2017.02.01 [00:00]"); 
int AlligatorHandle,EMA25Handle,EMA50Handle,OrderDirection=0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   AlligatorHandle = iAlligator(_Symbol,_Period,13,5,8,2,5,0,MODE_SMMA,PRICE_MEDIAN);
   EMA25Handle = iMA(_Symbol,_Period,25,0,MODE_SMMA,PRICE_CLOSE);
   EMA50Handle = iMA(_Symbol,_Period,50,0,MODE_SMMA,PRICE_CLOSE);
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   double HighBars[],LowBars[],ChangeDouble[],TestBars[],OrderVolume=0.1;
   int ChangeInt[],AlligatorPos,EMA25Pos,WaitBars=3;
   string EAComment;
      // Here we build the Comment that will serve as identifier for the Orders and Positions opended by this EA.
      EAComment = "AllMAFractEA: "+_Symbol+","+string(_Period);
   
   if(Wait && WaitBars<Bars(_Symbol,_Period,LastOpen,TimeLocal())){
     Wait = false;
   }
   
   
   AlligatorPos = AlligatorCheck(AlligatorHandle,AlligatorBars);
   EMA25Pos = EMA25Check(EMA25Handle,EMA25Bars,EMA25Perc);
   if(AlligatorPos==EMA25Pos && EMA25Pos==1 && !Wait && OrderDirection > -1){
      CopyLow(_Symbol,_Period,1,(BeforeFract+2),LowBars);
      for(int i=2;i<BeforeFract;i++){
         ArrayCopy(TestBars,LowBars,0,i-2,5);
         if(IsDownFractal(TestBars,5)&&!(Bars(_Symbol,_Period,LastOpen,TimeLocal()) == (i+1))){
            double OrderSL = MathRound((LowBars[i] - SymbolInfoDouble(_Symbol,SYMBOL_ASK) + SymbolInfoDouble(_Symbol,SYMBOL_BID) - 5*_Point)/_Point)*_Point;
            if(!trade.Buy(OrderVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),OrderSL,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+AddTP*_Point,EAComment)){
                 Alert("Buy Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
            } else {
               
               LastPrice = SymbolInfoDouble(_Symbol,SYMBOL_ASK);
               Wait = true;
               LastOpen = TimeLocal();
               if(NumOrders<1){
                  NumOrders++;
                  ArrayResize(OrderTicket,NumOrders);
                  ArrayResize(OrderType,NumOrders);
                  OrderTicket[0] = trade.RequestPosition();
                  OrderType[0] = 1;
                  OrderDirection =1;
               } else {
                  ArrayResize(ChangeDouble,NumOrders);
                  ArrayResize(ChangeInt,NumOrders);
                  NumOrders++;
                  ArrayCopy(ChangeDouble,OrderTicket,0,0);
                  ArrayCopy(ChangeInt,OrderType,0,0);
                  ArrayResize(OrderTicket,NumOrders);
                  ArrayResize(OrderType,NumOrders);
                  ArrayCopy(OrderTicket,ChangeDouble,0,0);
                  ArrayCopy(OrderType,ChangeInt,0,0);
                  ArrayFree(ChangeDouble);
                  ArrayFree(ChangeInt);
                  for(int j=0;j<NumOrders-1;j++){
                     if(!trade.PositionModify(OrderTicket[j],OrderSL,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+AddTP*_Point)){
                        Alert("Modify Buy SL Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                     }
                  }
               }
               int k=1;
               for(int j=0;j<PositionsTotal();j++){
                  PositionSelectByTicket(PositionGetTicket(j));
                  if(PositionGetString(POSITION_COMMENT)==EAComment){
                     if(!(k<NumOrders)){
                        OrderTicket[NumOrders-1] = PositionGetTicket(j);
                        // Alert("Position Ticket: ",OrderTicket[NumOrders-1]);
                        break;
                     }
                  k++;
                  }
               }
            }
            break;
         }
      }
      
   } else if(AlligatorPos==EMA25Pos && EMA25Pos==-1 && !Wait && OrderDirection < 1){
      CopyHigh(_Symbol,_Period,1,(BeforeFract+2),HighBars);
      for(int i=2;i<BeforeFract;i++){
         ArrayCopy(TestBars,HighBars,0,i-2,5);
         if(IsUpFractal(TestBars,5)&&(Bars(_Symbol,_Period,LastOpen,TimeLocal()) != (i+1))){
            double OrderSL = MathRound((HighBars[i] + SymbolInfoDouble(_Symbol,SYMBOL_ASK) - SymbolInfoDouble(_Symbol,SYMBOL_BID) + 5*_Point)/_Point)*_Point;
            if(!trade.Sell(OrderVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),OrderSL,SymbolInfoDouble(_Symbol,SYMBOL_BID)-AddTP*_Point,EAComment)){
                 Alert("Sell Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
            } else {
               LastPrice = SymbolInfoDouble(_Symbol,SYMBOL_BID);
               Wait = true;
               LastOpen = TimeLocal();
               if(NumOrders<1){
                  NumOrders++;
                  ArrayResize(OrderTicket,NumOrders);
                  ArrayResize(OrderType,NumOrders);
                  OrderTicket[0] = trade.RequestOrder();
                  OrderType[0] = -1;
                  OrderDirection =-1;
               } else {
                  ArrayResize(ChangeDouble,NumOrders);
                  ArrayResize(ChangeInt,NumOrders);
                  NumOrders++;
                  ArrayCopy(ChangeDouble,OrderTicket,0,0);
                  ArrayCopy(ChangeInt,OrderType,0,0);
                  ArrayResize(OrderTicket,NumOrders);
                  ArrayResize(OrderType,NumOrders);
                  ArrayCopy(OrderTicket,ChangeDouble,0,0);
                  ArrayCopy(OrderType,ChangeInt,0,0);
                  ArrayFree(ChangeDouble);
                  ArrayFree(ChangeInt);
                  
                  for(int j=0;j<NumOrders-1;j++){
                     if(!trade.PositionModify(OrderTicket[j],OrderSL,SymbolInfoDouble(_Symbol,SYMBOL_BID)-AddTP*_Point)){
                        Alert("Modify Sell SL Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                     }
                  }
               }
               
               int k=1;
               for(int j=0;j<PositionsTotal();j++){
                  PositionSelectByTicket(PositionGetTicket(j));
                  if(PositionGetString(POSITION_COMMENT)==EAComment){
                     if(!(k<NumOrders)){
                        OrderTicket[NumOrders-1] = PositionGetTicket(j);
                        // Alert("Position Ticket: ",OrderTicket[NumOrders-1]);
                        break;
                     }
                  k++;
                  }
               }
            }
            break;
         }
      }
      
   }
   
   if(NumOrders>0){
      if(OrderType[0]==1){
         if(AlligatorPos < 1 || EMA25Pos < 1) {
            if(SymbolInfoDouble(_Symbol,SYMBOL_BID)<LastPrice) {
               for(int i=0;i<NumOrders;i++){
                  if(!trade.PositionModify(OrderTicket[i],LastSL,LastPrice+10*_Point)){
                     Alert("Modify Buy TP Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                  }
               }
            } else {
               for(int i=0;i<NumOrders;i++){
                  if(!trade.PositionClose(OrderTicket[i],3)){
                     Alert("Close Buy Position Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                  }
               }
            }
            NumOrders = 0;
            ArrayFree(OrderTicket);
            LastPrice=0;
            LastSL=0;
         }
      } else {
         if(AlligatorPos > -1 || EMA25Pos > -1){
            if(SymbolInfoDouble(_Symbol,SYMBOL_ASK)>LastPrice) {
               for(int i=0;i<NumOrders;i++){
                  if(!trade.PositionModify(OrderTicket[i],LastSL,LastPrice-10*_Point)){
                     Alert("Modify Sell TP Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                  }
               }
            } else {
               for(int i=0;i<NumOrders;i++){
                  if(!trade.PositionClose(OrderTicket[i],3)){
                     Alert("Close Sell Position Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                  }
               }
            }
            NumOrders = 0;
            ArrayFree(OrderTicket);
            LastPrice=0;
            LastSL=0;
         }
      }
   }
   
   
}
//+------------------------------------------------------------------+
// We check the condition of the Alligator Indicator for a number of |
// previous bars. It returns 1 if buy, -1 if sell or 0 if neither.   |
//+------------------------------------------------------------------+
int AlligatorCheck(int AllHandle,int CheckBars=1){
   double Jaws[],Lips[],Teeth[];
   // ArrayResize(Jaws,CheckBars);
   // ArrayResize(Lips,CheckBars);
   // ArrayResize(Teeth,CheckBars);
   
   CopyBuffer(AllHandle,0,1,CheckBars,Jaws);
   CopyBuffer(AllHandle,1,1,CheckBars,Teeth);
   CopyBuffer(AllHandle,2,1,CheckBars,Lips);
   
   bool Up=true,Down=true;
   
   for(int i=0;i<CheckBars;i++) {
      // 
      if(!(Teeth[i] < Lips[i] && Teeth[i] > Jaws[i])){
         Up = false;
      }
      //
      if(!(Teeth[i] > Lips[i] && Teeth[i] < Jaws[i])){
         Down = false;
      }
      if(!(Up||Down)){
         return(0);
      }
   }
   if(Up){
      return(1);
   }
   if(Down){
      return(-1);
   }
   return(-15);
   
}
//+------------------------------------------------------------------+
// We check if the closing price has mostly stayed on one side of a  |
//50EMA for a number of bars.                                        |
//+------------------------------------------------------------------+
int EMA25Check(int EMA25Hand,int CheckBars=10,double PercCheck=0.9){
   double EMA25[],ClosePrice[],UpPerc;
   CopyBuffer(EMA25Hand,0,1,CheckBars,EMA25);
   CopyClose(_Symbol,_Period,1,CheckBars,ClosePrice);
   
   int Up=0;
   
   for(int i=0;i<CheckBars;i++) {
      // 
      if(EMA25[i] < ClosePrice[i]){
         Up++;
      }
   }
   
   UpPerc = Up/double(CheckBars);
   if(UpPerc>PercCheck){
      return(1);
   }
   UpPerc = 1-UpPerc;
   if(1-Up/double(CheckBars)>PercCheck){
      return(-1);
   }
   return(0);
   
}
  
  
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bearish Fractal   |
//for the whole sequence.                                            |
//+------------------------------------------------------------------+
bool IsUpFractal(double &Prices[],int barsNum=5)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   // Print(Prices[0],Prices[1],Prices[2],Prices[3],Prices[4]);
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] > Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bullish Fractal   |
//for the whole sequence.                                            |
//+------------------------------------------------------------------+
bool IsDownFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] < Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}