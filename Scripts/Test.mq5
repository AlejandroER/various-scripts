//+------------------------------------------------------------------+
//|                                                         Test.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <TradeAdditions.mqh>
#include <ZZFibFuncs.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   MqlDateTime Test1;
   
   TimeCurrent(Test1);
   
   Alert(StructToTime(Test1));
   
   Test1.hour = 19;
   
   Alert(StructToTime(Test1));
}
//+------------------------------------------------------------------+
