//+------------------------------------------------------------------+
//|                                                  TradingTest.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   ulong OrderTicket;
   int magic_number = 1234567;
   long OrderType;
   MqlTradeRequest request={0}; 
   request.action=TRADE_ACTION_PENDING;         // setting a pending order 
   request.magic=magic_number;                  // ORDER_MAGIC 
   request.symbol=_Symbol;                      // symbol 
   request.volume=0.01;                          // volume in 0.1 lots 
   request.sl=SymbolInfoDouble(_Symbol, SYMBOL_BID);
   request.tp=SymbolInfoDouble(_Symbol, SYMBOL_ASK)+20*0.00001;                                // Take Profit is not specified      
//--- form the order type 
   request.type=ORDER_TYPE_BUY_STOP;                // order type 
//--- form the price for the pending order 
   request.price=SymbolInfoDouble(_Symbol, SYMBOL_ASK)+10*0.00001;  // open price 
//--- send a trade request 
   MqlTradeResult result={0}; 
   OrderSend(request,result);
   OrderTicket = result.order;
   
   OrderType = OrderGetInteger(ORDER_TYPE);
   if(OrderType == ORDER_TYPE_BUY_STOP){
      Alert("BuyStop");
   } else if(OrderType == ORDER_TYPE_SELL_STOP){
      Alert("SellStop");
   } else if(OrderType == ORDER_TYPE_BUY){
      Alert("Buy");
   } else if(OrderType == ORDER_TYPE_SELL){
      Alert("Sell");
   }
   Alert(result.retcode);
   Alert(result.comment);
   Alert("Ticket # ",OrderTicket);
   
   
   
}
//+------------------------------------------------------------------+
