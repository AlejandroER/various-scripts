//+------------------------------------------------------------------+
//|                                                PriceActionEA.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
#include <Trade\Trade.mqh>
// input int StopLossType=1;
input int TakeProfitType=1;
input double CheckHammer=0.5;
input int AddStopLoss=1;
input int MAPeriod=60;
input int MASide=3;
datetime PrevEntry=StringToTime("2017.02.01 [00:00]");
CTrade trade;
int MAHandle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   
   if(CheckHammer>1){
      return(INIT_PARAMETERS_INCORRECT);
   }
   MAHandle=iMA(_Symbol,_Period,MAPeriod,0,MODE_SMMA,PRICE_MEDIAN);
   if(MAHandle==INVALID_HANDLE){
      printf("Error creating MA indicator");
      return(INIT_FAILED);
   }
   if(MASide>MAPeriod){
      return(INIT_PARAMETERS_INCORRECT);
   }
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
   
   
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   double OpenPrice[3],ClosePrice[3],HighPrice[3],LowPrice[3],StopLoss=0,TakeProfit=0,MABars[],CloseTest[];
   MqlDateTime CurrentTime;
   TimeToStruct(TimeLocal(),CurrentTime);
   CopyClose(_Symbol,_Period,1,3,ClosePrice);
   CopyHigh(_Symbol,_Period,1,3,HighPrice);
   CopyLow(_Symbol,_Period,1,3,LowPrice);
   CopyOpen(_Symbol,_Period,1,3,OpenPrice);
   CopyBuffer(MAHandle,0,1,MASide,MABars);
   string EAComment;
   bool MATest;
   
      // Here we build the Comment that will serve as identifier for the Orders and Positions opended by this EA.
      EAComment = "HammerEA: "+_Symbol+","+string(_Period);
   if(CurrentTime.hour>10 && CurrentTime.hour<22 && Bars(_Symbol,_Period,PrevEntry,TimeLocal())>0){
   CopyClose(_Symbol,_Period,1,MASide,CloseTest);
      if(MathAbs(HighPrice[2]-ClosePrice[2]) < CheckHammer*(ClosePrice[2]-OpenPrice[2]) && (OpenPrice[2]-LowPrice[2])>(ClosePrice[2]-OpenPrice[2])){
         MATest = true;
         for(int i=0;i<MASide;i++){
            MATest = MATest && CloseTest[i]>MABars[i];
            if(!MATest) break;
         }
         if(OpenPrice[1]>ClosePrice[2] && OpenPrice[0]>ClosePrice[2] && (ClosePrice[2]-LowPrice[2])<=0.00030 && MATest){
            //if(StopLossType==1){
            //   StopLoss = ClosePrice[2]-
            //} else if(StopLossType==2){
            //   
            //}else {
            //   
            //}
            if(TakeProfitType==1){
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+1*_Point;
            } else if(TakeProfitType==2){
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+4*_Point;
            }else {
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+(ClosePrice[2]-LowPrice[2]);
            }
            StopLoss = LowPrice[2]-SymbolInfoDouble(_Symbol,SYMBOL_ASK)+SymbolInfoDouble(_Symbol,SYMBOL_BID)-AddStopLoss*_Point;
            if(!trade.Buy(0.1,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),StopLoss,TakeProfit,EAComment)){
               Alert("Buy Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
            } else {
               PrevEntry = TimeLocal();
            }
         }
      }
      
      if(MathAbs(LowPrice[2]-ClosePrice[2]) < CheckHammer*(OpenPrice[2]-ClosePrice[2]) && (HighPrice[2] - OpenPrice[2])>(OpenPrice[2] - ClosePrice[2])){
         MATest = true;
         for(int i=0;i<MASide;i++){
            MATest = MATest && CloseTest[i]<MABars[i];
            if(!MATest) break;
         }
         if(OpenPrice[1]<ClosePrice[2] && OpenPrice[0]<ClosePrice[2] && (HighPrice[2]-ClosePrice[2])<=0.00030 && MATest){
            if(TakeProfitType==1){
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_BID)-1*_Point;
            } else if(TakeProfitType==2){
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_BID)-4*_Point;
            }else {
               TakeProfit = SymbolInfoDouble(_Symbol,SYMBOL_BID)-(HighPrice[2]-ClosePrice[2]);
            }
            StopLoss = HighPrice[2]+SymbolInfoDouble(_Symbol,SYMBOL_ASK)-SymbolInfoDouble(_Symbol,SYMBOL_BID)+AddStopLoss*_Point;
            if(!trade.Sell(0.1,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),StopLoss,TakeProfit,EAComment)){
               Alert("Sell Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
            } else {
               PrevEntry = TimeLocal();
            }
            
         }
      }
   
   
   }
}
//+------------------------------------------------------------------+
