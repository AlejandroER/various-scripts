//+------------------------------------------------------------------+
//|                                                      ForTest.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
   int i,handle,TestNum = 20;
   double MaTest[3];
   
   handle = iMA(_Symbol,_Period,25,0,MODE_SMA,PRICE_CLOSE);
   CopyBuffer(handle,0,1,3,MaTest);
   Alert(SymbolInfoDouble(_Symbol, SYMBOL_BID),SymbolInfoDouble(_Symbol, SYMBOL_ASK));
   //for(i=0;i<TestNum;i++)
   //{
   //   Alert(i," ; ",TestNum);
   //   i = i+1;
   //   TestNum = TestNum-1;
   //}
   //Alert(i," ; ",TestNum);
  }
//+------------------------------------------------------------------+
