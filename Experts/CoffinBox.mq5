//+------------------------------------------------------------------+
//|                                                    CoffinBox.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property version   "1.00"
#include <Trade\Trade.mqh>
#include <Signals.mqh>
#include <TradeAdditions.mqh>
//--- input parameters
enum OpType{
   Perc100 = 1,
   Perc60 = 2,
   Both = 3
};
enum OpTime{
   First = 1,
   BothTimes = 2
};
input int      PrevBars1=71;
input int      StartHour1=10;
input int      StopHours1=2;
input double   MaxRiskPerc=0.02;
input double   MaxRisk=10;
input int      PrevBars2=61;
input int      StartHour2=15;
input int      StopHours2=1;
input OpType   FullOperation = Perc100;
input OpTime   BothOperations = BothTimes;
input int      MAPeriod = 250;
input int      MAChkBars = 12;

//--- Global Variables
double PivotR2Buffer[],PivotS2Buffer[],PivotR3Buffer[],PivotS3Buffer[];
double HighestValue1,LowestValue1,HighestValue2,LowestValue2;
int Pivothandle,MAHandle;
ulong Position1Ticket[],Position2Ticket[];
bool Position1Open=false,Position2Open=false,Position1Ready=false,Position2Ready=false;
CTrade trade;
datetime StartTime1=TimeCurrent(),StartTime2=TimeCurrent();
string EAComment;
int PlotCount=0;
string PlotName = "CBox";
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
//---
   if(PrevBars1<1||PrevBars2<1||StopHours1<1||StopHours2<1||StartHour1<1||StartHour2<1||MaxRiskPerc<0||MaxRiskPerc>1) return(INIT_PARAMETERS_INCORRECT);
   ArraySetAsSeries(PivotR2Buffer,true);
   ArraySetAsSeries(PivotR3Buffer,true);
   ArraySetAsSeries(PivotS2Buffer,true);
   ArraySetAsSeries(PivotS3Buffer,true);
//---
   EAComment = "CoffinBoxExpert: "+_Symbol;
   ulong MagicNumber;
   MagicNumber = 1011000+_Period;
   trade.SetExpertMagicNumber(MagicNumber);
//---
   Pivothandle = iCustom(_Symbol,_Period,"PivotPoints");
   MAHandle = iMA(_Symbol,_Period,250,0,MODE_SMMA,PRICE_TYPICAL);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   MqlDateTime TimeCheck;
   TimeCurrent(TimeCheck);
   
   if(TimeCheck.hour == StartHour1-1 && TimeCheck.min >55 && !Position1Ready){
      int barShift = iHighest(_Symbol,_Period,MODE_HIGH,PrevBars1,1);
      if(barShift == -1) return;
      HighestValue1 = iHigh(_Symbol,_Period,barShift);
      barShift = iLowest(_Symbol,_Period,MODE_LOW,PrevBars1,1);
      if(barShift == -1) return;
      LowestValue1 = iLow(_Symbol,_Period,barShift);
      Position1Ready = true;
      TimeCheck.hour = StartHour1;
      TimeCheck.min = 0;
      TimeCheck.sec = 0;
      StartTime1 = StructToTime(TimeCheck);
      PlotCoffinBox(PlotName+IntegerToString(PlotCount),HighestValue1,LowestValue1,StopHours1*12);
      PlotCount = PlotCount + 1;
      return;
   }
   
   if(TimeCheck.hour == StartHour2-1 && TimeCheck.min >55 && !Position2Ready){
      int barShift = iHighest(_Symbol,_Period,MODE_HIGH,PrevBars2,1);
      if(barShift == -1) return;
      HighestValue2 = iHigh(_Symbol,_Period,barShift);
      barShift = iLowest(_Symbol,_Period,MODE_LOW,PrevBars2,1);
      if(barShift == -1) return;
      LowestValue2 = iLow(_Symbol,_Period,barShift);
      Position2Ready = true;
      TimeCheck.hour = StartHour2;
      TimeCheck.min = 0;
      TimeCheck.sec = 0;
      StartTime2 = StructToTime(TimeCheck);
      PlotCoffinBox(PlotName+IntegerToString(PlotCount),HighestValue2,LowestValue2,StopHours2*12);
      PlotCount = PlotCount + 1;
      return;
   }
   
   if((Position1Ready || Position1Open) && (StructToTime(TimeCheck)-StartTime1)/3600 > StopHours1){
      Position1Open = false;
      Position1Ready = false;
      return;
   }
   
   if((Position2Ready || Position2Open) && (StructToTime(TimeCheck)-StartTime2)/3600 > StopHours2){
      Position2Open = false;
      Position2Ready = false;
      return;
   }
   
   if(StructToTime(TimeCheck) > StartTime1 && Position1Ready && !Position1Open){
      CopyBuffer(Pivothandle,3,0,1,PivotR2Buffer);
      CopyBuffer(Pivothandle,4,0,1,PivotS2Buffer);
      bool MAEntry = false;
      int MATest;
      if(MAChkBars == 0){
         MAEntry = true;
         MATest = 0;
      } else {
         MATest = SteadyCloseMA(MAHandle,0,MAChkBars);
      }
      if(2*HighestValue1-LowestValue1 > PivotR2Buffer[0] || 2*LowestValue1-HighestValue1 < PivotS2Buffer[0]){
         Position1Ready = false;
         return;
      }
      if(LevelBreak(HighestValue1) == 1 && (MATest == 1 || MAEntry)){
         double SL,TP,TradeVolume;
         SL = LowestValue1;
         TradeVolume = EstimProfit(SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,MaxRisk,false,0.0);
         if(TradeVolume < 0.01){
            //Position1Ready = false;
            //break;
            TradeVolume = 0.01;
         }
         if(FullOperation == Perc100){
            ArrayResize(Position1Ticket,1,0);
            TP = 2*HighestValue1-LowestValue1;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
         } else if(FullOperation == Perc60)
         {
            ArrayResize(Position1Ticket,1,0);
            TP = HighestValue1 + (HighestValue1-LowestValue1)*0.6;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
         } else {
            ArrayResize(Position1Ticket,2,0);
            TradeVolume = MathFloor(TradeVolume*50)/100;
            if(TradeVolume < 0.01) TradeVolume = 0.01;
            TP = 2*HighestValue1-LowestValue1;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
            TP = HighestValue1 + (HighestValue1-LowestValue1)*0.6;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position1Ticket[1] = trade.ResultOrder();
         }
         Position1Ready = false;
         Position1Open = true;
      }
      if(LevelBreak(LowestValue1) == -1 && (MATest == -1 || MAEntry)){
         double SL,TP,TradeVolume;
         SL = HighestValue1;
         TradeVolume = EstimProfit(SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,MaxRisk,false,0.0);
         if(TradeVolume < 0.01){
            //Position1Ready = false;
            //break;
            TradeVolume = 0.01;
         }
         if(FullOperation == Perc100){
            ArrayResize(Position1Ticket,1,0);
            TP = 2*LowestValue1-HighestValue1;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
         } else if(FullOperation == Perc60)
         {
            ArrayResize(Position1Ticket,1,0);
            TP = LowestValue1-(HighestValue1-LowestValue1)*0.6;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
         } else {
            ArrayResize(Position1Ticket,2,0);
            TradeVolume = MathFloor(TradeVolume*50)/100;
            if(TradeVolume < 0.01) TradeVolume = 0.01;
            TP = 2*LowestValue1-HighestValue1;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position1Ticket[0] = trade.ResultOrder();
            TP = LowestValue1-(HighestValue1-LowestValue1)*0.6;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position1Ticket[1] = trade.ResultOrder();
         }
         Position1Ready = false;
         Position1Open = true;
      }
   }
   
   if(StructToTime(TimeCheck) > StartTime2 && Position2Ready && !Position2Open && BothOperations == BothTimes){
      CopyBuffer(Pivothandle,3,0,1,PivotR2Buffer);
      CopyBuffer(Pivothandle,4,0,1,PivotS2Buffer);
      bool MAEntry = false;
      int MATest;
      if(MAChkBars == 0){
         MAEntry = true;
         MATest = 0;
      } else {
         MATest = SteadyCloseMA(MAHandle,0,MAChkBars);
      }
      if(2*HighestValue2-LowestValue2 > PivotR2Buffer[0] || 2*LowestValue2-HighestValue2 < PivotS2Buffer[0]){
         Position2Ready = false;
         return;
      }
      if(LevelBreak(HighestValue2) == 1 && (MATest == 1 || MAEntry)){
         double SL,TP,TradeVolume;
         SL = LowestValue2;
         TradeVolume = EstimProfit(SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,MaxRisk,false,0.0);
         if(TradeVolume < 0.01){
            //Position2Ready = false;
            //break;
            TradeVolume = 0.01;
         }
         if(FullOperation == Perc100){
            ArrayResize(Position2Ticket,1,0);
            TP = 2*HighestValue2-LowestValue2;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
         } else if(FullOperation == Perc60)
         {
            ArrayResize(Position2Ticket,1,0);
            TP = HighestValue2 + (HighestValue2-LowestValue2)*0.6;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
         } else {
            ArrayResize(Position2Ticket,2,0);
            TradeVolume = MathFloor(TradeVolume*50)/100;
            if(TradeVolume < 0.01) TradeVolume = 0.01;
            TP = 2*HighestValue2-LowestValue2;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
            TP = HighestValue1 + (HighestValue2-LowestValue2)*0.6;
            trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
            Position2Ticket[1] = trade.ResultOrder();
         }
         Position2Ready = false;
         Position2Open = true;
      }
      if(LevelBreak(LowestValue2) == -1 && (MATest == -1 || MAEntry)){
         double SL,TP,TradeVolume;
         SL = HighestValue2;
         TradeVolume = EstimProfit(SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,MaxRisk,false,0.0);
         if(TradeVolume < 0.01){
            //Position2Ready = false;
            //break;
            TradeVolume = 0.01;
         }
         if(FullOperation == Perc100){
            ArrayResize(Position2Ticket,1,0);
            TP = 2*LowestValue2-HighestValue2;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
         } else if(FullOperation == Perc60)
         {
            ArrayResize(Position2Ticket,1,0);
            TP = LowestValue2-(HighestValue2-LowestValue2)*0.6;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
         } else {
            ArrayResize(Position2Ticket,2,0);
            TradeVolume = MathFloor(TradeVolume*50)/100;
            if(TradeVolume < 0.01) TradeVolume = 0.01;
            TP = 2*LowestValue2-HighestValue2;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position2Ticket[0] = trade.ResultOrder();
            TP = LowestValue2-(HighestValue2-LowestValue2)*0.6;
            trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
            Position2Ticket[1] = trade.ResultOrder();
         }
         Position2Ready = false;
         Position2Open = true;
      }
   }
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade(){
//---
   int RemTickets;
   
   RemTickets = CheckPosClose(Position1Ticket);
   if(RemTickets == 0 && Position1Open){
      ArrayFree(Position1Ticket);
      Position1Open = false;
   }
   
   RemTickets = CheckPosClose(Position2Ticket);
   if(RemTickets == 0 && Position2Open){
      ArrayFree(Position1Ticket);
      Position2Open = false;
   }
   
}
//+------------------------------------------------------------------+
//| Check if any Position has closed.                                |
//+------------------------------------------------------------------+
int CheckPosClose(ulong &TicketArray[]){
   int Remaining=0,AllPositions = PositionsTotal();
   ulong RemTickets[];
   bool Remains;
   for(int i=0;i<ArraySize(TicketArray);i++){
      Remains = false;
      for(int j=0;j<AllPositions;j++){
         if(PositionGetTicket(j) == TicketArray[i]) Remains=true;
      }
      if(Remains){
         Remaining = Remaining+1;
         ArrayResize(RemTickets,Remaining,0);
         RemTickets[Remaining-1] = TicketArray[i];
      }
   }
   if(Remaining==0){
      ArrayFree(TicketArray);
   } else {
      ArrayResize(TicketArray,Remaining,0);
      ArrayCopy(TicketArray,RemTickets);
   }
   return(Remaining);
}
//+------------------------------------------------------------------+
//| Create horizontal lines for CoffinBox                            |
//+------------------------------------------------------------------+
int PlotCoffinBox(string Name,double PriceLvlUp,double PriceLvlDown,int FwdBars,int BckBars=0,int CenterBar=0,color InnerColor=clrAqua,color OuterColor=clrYellow,int InnerWidth=2,int OuterWidth=1){
   double BoxSize = PriceLvlUp - PriceLvlDown;
   // We determine the time stamps for the edges of the rectangles
   datetime Start = iTime(_Symbol,_Period,CenterBar+BckBars)+5*60;
   datetime End = iTime(_Symbol,_Period,CenterBar) + 5*60*FwdBars;
   // We plot the inner box and return -1 if it fails
   if(PlotBox(Name+"-1",PriceLvlUp,PriceLvlDown,Start,End,InnerWidth,InnerColor) == -1){
      if(PlotBox(Name+"1",PriceLvlUp,PriceLvlDown,Start,End,InnerWidth,InnerColor) == -1) return(-1);
   }
   // We plot the outer box and return -1 if it fails
   if(PlotBox(Name+"-2",PriceLvlUp+BoxSize,PriceLvlDown-BoxSize,Start,End,OuterWidth,OuterColor) == -1){
      if(PlotBox(Name+"2",PriceLvlUp+BoxSize,PriceLvlDown-BoxSize,Start,End,OuterWidth,OuterColor) == -1) return(-1);
   }
   return(0);
   
}
//+------------------------------------------------------------------+
//| Create horizontal line of set color and name                     |
//+------------------------------------------------------------------+
int PlotHSegment(string Name,double PriceLvl,datetime Start,datetime End,int Width=1,color SegColor=clrRed){
   // Create segment and return -1 if it fails
   if(!ObjectCreate(0,Name,OBJ_TREND,0,Start,PriceLvl,End,PriceLvl)) return(-1);
   // Change segment color
   ObjectSetInteger(0,Name,OBJPROP_COLOR,SegColor);
   // Change Segment width
   ObjectSetInteger(0,Name,OBJPROP_WIDTH,Width);
   return(0);
}
//+------------------------------------------------------------------+
//| Create rectangle of set color and name                           |
//+------------------------------------------------------------------+
int PlotBox(string Name,double PriceLvl1,double PriceLvl2,datetime Start,datetime End,int Width=1,color SegColor=clrRed){
   // Create segment and return -1 if it fails
   if(!ObjectCreate(0,Name,OBJ_RECTANGLE,0,Start,PriceLvl1,End,PriceLvl2)) return(-1);
   // Change segment color
   ObjectSetInteger(0,Name,OBJPROP_COLOR,SegColor);
   // Change Segment width
   ObjectSetInteger(0,Name,OBJPROP_WIDTH,Width);
   // Ensure that the rectangle is unfilled
   ObjectSetInteger(0,Name,OBJPROP_FILL,false);
   return(0);
}