//+------------------------------------------------------------------+
//|                                                AUDUSDExtract.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+


void OnStart()
   {
      
      MqlDateTime CurTime,ExtractTime;
      datetime CandleTime[];
      double CeroClose[1],TwoThreeClose[1],FourClose[1],ma7[2],ma25[2],ma50[2],ma100[2];
      int i,FileHandle,handle1,handle2,handle3,handle4,DaysYear,CeroCandle;
      string CurDate;
      
      handle1 = iMA(_Symbol,_Period,100,0,MODE_SMA,PRICE_CLOSE);
      handle2 = iMA(_Symbol,_Period,50,0,MODE_SMA,PRICE_CLOSE);
      handle3 = iMA(_Symbol,_Period,25,0,MODE_SMA,PRICE_CLOSE);
      handle4 = iMA(_Symbol,_Period,7,0,MODE_SMA,PRICE_CLOSE);
      
      TimeToStruct(TimeCurrent(),CurTime);
      
      DaysYear = CurTime.day_of_year;
      
      FileHandle = FileOpen("TestData.csv",FILE_READ|FILE_WRITE|FILE_CSV);
      FileWrite(FileHandle,"Date","Close00","Close04","MA7","MA25","MA50","MA100","Close23","MA7-23","MA25-23","MA50-23","MA100-23");
      
      CeroCandle = CurTime.hour - 24;
      
      for(i=0;i < DaysYear;i++)
         {
            
            CeroCandle = CeroCandle + 24;
            CopyClose(_Symbol,_Period,CeroCandle,1,CeroClose);
            CopyBuffer(handle1,0,CeroCandle,2,ma100);
            CopyBuffer(handle2,0,CeroCandle,2,ma50);
            CopyBuffer(handle3,0,CeroCandle,2,ma25);
            CopyBuffer(handle4,0,CeroCandle,2,ma7);
            CopyClose(_Symbol,_Period,CeroCandle-4,1,FourClose);
            CopyClose(_Symbol,_Period,CeroCandle+1,1,TwoThreeClose);
            
            CopyTime(_Symbol,_Period,CeroCandle,1,CandleTime);
            TimeToStruct(CandleTime[0],ExtractTime);
            if(ExtractTime.day_of_week == 1)
              {
               DaysYear = DaysYear-2;
              }
            CurDate = IntegerToString(ExtractTime.year)+IntegerToString(ExtractTime.mon,2,'0')+IntegerToString(ExtractTime.day,2,'0');
            FileWrite(FileHandle,CurDate,CeroClose[0],FourClose[0],ma7[1],ma25[1],ma50[1],ma100[1],TwoThreeClose[0],ma7[0],ma25[0],ma50[0],ma100[0]);
            
            if(i < 5)
              {
               Alert(CeroClose[0]," ; ",ma7[1]," ; ",FourClose[0]);
              }
         }
      
      
      IndicatorRelease(handle1);
      IndicatorRelease(handle2);
      IndicatorRelease(handle3);
      IndicatorRelease(handle4);
      
      FileClose(FileHandle);
      //ArraySetAsSeries(MyOpen,true);
      
      
      
   }
//+------------------------------------------------------------------+
