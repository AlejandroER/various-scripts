//+------------------------------------------------------------------+
//|                                                        OBVMA.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
#include <Trade\Trade.mqh>
//--- input parameters
input int      MAPeriod=14;
input int      CCIPeriod=14;
input double   Volume=0.1;
input int      MinBars=5;
input int      FractNum=3;
int MAHandle,OBVHandle,CCIHandle,ATRHandle;
CTrade trade;
int OrderType = 0;
ulong OrderTicket = 0;
datetime CurrentCross,LastCross=StringToTime("2017.02.01 [00:00]"); 
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   
   OBVHandle = iOBV(_Symbol,_Period,VOLUME_TICK);
   MAHandle = iMA(_Symbol,_Period,MAPeriod,0,MODE_SMMA,OBVHandle);
   CCIHandle = iCCI(_Symbol,_Period,CCIPeriod,PRICE_TYPICAL);
   ATRHandle = iATR(_Symbol,_Period,14);
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
   
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   
   double OBVBuffer[2],MABuffer[2],CCIBuffer[2],ATRBuffer[1],SL=0.0,TP=0.0;
   CopyBuffer(OBVHandle,0,1,2,OBVBuffer);
   CopyBuffer(CCIHandle,0,1,2,CCIBuffer);
   CopyBuffer(MAHandle,0,1,2,MABuffer);
   CopyBuffer(ATRHandle,0,0,1,ATRBuffer);
   string EAComment;
   int AllPositions;
   ulong TestOrderTicket;
   // Here we build the Comment that will serve as identifier for the Orders and Positions opended by this EA.
   EAComment = "OBVMAExpert: "+_Symbol+","+string(_Period);
   AllPositions = PositionsTotal();
   
   if(AllPositions == 0 && (OrderType != 0 || OrderTicket != 0)){
      OrderTicket = 0;
      OrderType = 0;
   }
   
   if(OrderType != 0 && OrderTicket == 0){
      for(int i=0;i<AllPositions;i++){
         TestOrderTicket = PositionGetTicket(i);
         if(!PositionSelectByTicket(TestOrderTicket)){
            Sleep(1000);
            if(!PositionSelectByTicket(TestOrderTicket)){
               TestOrderTicket = 0;
               Sleep(1000);
               return;
            }
         }
         // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
         if(PositionGetString(POSITION_COMMENT) == EAComment){
            OrderTicket = TestOrderTicket;
            break;
         }
      }
      if(OrderTicket == 0) OrderType = 0;
      
   } else if(OrderType == 0 && OrderTicket != 0){
      if(!PositionSelectByTicket(OrderTicket)){
         Sleep(1000);
         if(!PositionSelectByTicket(OrderTicket)){
            OrderTicket = 0;
            Sleep(1000);
            return;
         }
      }
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY){
         OrderType = 1;
      } else if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL){
         OrderType = -1;
      }
      
   } else {
      if(OrderType > 0 && OrderTicket > 0){
         if(MABuffer[1]>OBVBuffer[1] || CCIBuffer[1] < 0){
            if(!trade.PositionClose(OrderTicket)){
               Sleep(1000);
               if(!trade.PositionClose(OrderTicket)){
                  Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
                  Sleep(1000);
                  return;
               }
            }
            if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
               Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               return;
            }
         }
         
         
      } else if(OrderType < 0 && OrderTicket > 0){
         if(MABuffer[1]<OBVBuffer[1] || CCIBuffer[1] > 0){
            if(!trade.PositionClose(OrderTicket)){
               Sleep(1000);
               if(!trade.PositionClose(OrderTicket)){
                  Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
                  Sleep(1000);
                  return;
               }
            }
            if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
               Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               return;
            }
         }
         
         
      } else {
         if(OBVBuffer[0]<=MABuffer[0]&&OBVBuffer[1]>MABuffer[1]&&CCIBuffer[1]>0&&CCIBuffer[1]<100){         // Buy Condition: OBV crossover over MA and 0<CCI<100
            CurrentCross = TimeLocal();
            if(Bars(_Symbol,_Period,LastCross,CurrentCross)<MinBars){
            
            } else {
               double PriceBuffer[5];
               bool FractalTrue=false;
               double FractalPrice = 0;
               for(int i=1;i<(FractNum+1);i++){
                  CopyLow(_Symbol,_Period,i,5,PriceBuffer);
                  if(IsDownFractal(PriceBuffer,5)){
                     FractalPrice = PriceBuffer[2];
                     FractalTrue = true;
                     break;
                  }
               }
               if(FractalTrue && (SymbolInfoDouble(_Symbol,SYMBOL_BID)-FractalPrice)<2*ATRBuffer[0]){
                  SL = FractalPrice-10*_Point;
               } else {
                  SL = SymbolInfoDouble(_Symbol,SYMBOL_BID)-2*ATRBuffer[0];
               }
               
               TP = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+10*ATRBuffer[0];
               if(!trade.Buy(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment)){
                  Sleep(1000);
                  if(!trade.Buy(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment)){
                     Alert("Error Opening Buy Position with Error Code "+string(trade.ResultRetcode()));
                     Sleep(1000);
                     return;
                  }
               }
               if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
                  Alert("Error Opening Buy Position with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
                  ExpertRemove();
               }
               OrderType = 1;
               OrderTicket = trade.RequestPosition();
            }
            LastCross = CurrentCross;
            
            
         } else if(OBVBuffer[0]>=MABuffer[0]&&OBVBuffer[1]<MABuffer[1]&&CCIBuffer[1]<0&&CCIBuffer[1]>-100){ // Sell Condition: OBV crossover under MA and 0>CCI>-100
            CurrentCross = TimeLocal();
            if(Bars(_Symbol,_Period,LastCross,CurrentCross)<MinBars){
            
            } else {
               double PriceBuffer[5];
               bool FractalTrue=false;
               double FractalPrice = 0;
               for(int i=1;i<(FractNum+1);i++){
                  CopyHigh(_Symbol,_Period,i,5,PriceBuffer);
                  if(IsUpFractal(PriceBuffer,5)){
                     FractalPrice = PriceBuffer[2];
                     FractalTrue = true;
                     break;
                  }
               }
               if(FractalTrue && (SymbolInfoDouble(_Symbol,SYMBOL_BID)-FractalPrice)<2*ATRBuffer[0]){
                  SL = FractalPrice+10*_Point;
               } else {
                  SL = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+2*ATRBuffer[0];
               }
               TP = SymbolInfoDouble(_Symbol,SYMBOL_BID)-10*ATRBuffer[0];
               if(!trade.Sell(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment)){
                  Sleep(1000);
                  if(!trade.Sell(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment)){
                     Alert("Error Opening Sell Position with Error Code "+string(trade.ResultRetcode()));
                     Sleep(1000);
                     return;
                  }
               }
               if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
                  Alert("Error Opening Sell Position with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
                  ExpertRemove();
               }
            }
            OrderType = -1;
            OrderTicket = trade.RequestPosition();
         }
         LastCross = CurrentCross;
         
      }
      
   }
}
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bearish Fractal for the whole sequence.
bool IsUpFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] > Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bullish Fractal for the whole sequence.
bool IsDownFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] < Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}