//+------------------------------------------------------------------+
//|                                            ExtractPerceptron.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property script_show_inputs
//--- input parameters
input int      PrevBars=200;
input string   Volume="OBV";
input int      Skip=25;
input int      Entries=10000;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   int VolumeHandle,FileHandle;
   double LowPrice[],HighPrice[],OpenPrice[],ClosePrice[],VolumeBuffer[],CenterPoint;
   string FileLine;
   //if(Volume=="OBV"){
      VolumeHandle = iOBV(_Symbol,_Period,VOLUME_TICK);
   //}
   
   FileHandle = FileOpen("PerceptronTestData.csv",FILE_READ|FILE_WRITE|FILE_CSV,",");
   FileLine = "\"";
   for(int i=0;i<PrevBars;i++){
      FileLine = FileLine+"Low"+IntegerToString(PrevBars-i)+"\",\"";
      FileLine = FileLine+"High"+IntegerToString(PrevBars-i)+"\",\"";
      FileLine = FileLine+"Open"+IntegerToString(PrevBars-i)+"\",\"";
      FileLine = FileLine+"Close"+IntegerToString(PrevBars-i)+"\",\"";
      FileLine = FileLine+"Volume"+IntegerToString(PrevBars-i)+"\",\"";
   }
   FileLine = FileLine+"Fut"+IntegerToString(5)+"\",\"";
   FileLine = FileLine+"Fut"+IntegerToString(10)+"\",\"";
   FileLine = FileLine+"Fut"+IntegerToString(25)+"\",\"";
   FileLine = FileLine+"Fut"+IntegerToString(50)+"\"";
   FileWrite(FileHandle,FileLine);
   
   
   for(int j=0;j<Entries;j++){
      CopyBuffer(VolumeHandle,0,1+j*Skip,PrevBars,VolumeBuffer);
      CopyClose(_Symbol,_Period,1+j*Skip,PrevBars+50,ClosePrice);
      CopyOpen(_Symbol,_Period,1+j*Skip,PrevBars+50,OpenPrice);
      CopyHigh(_Symbol,_Period,1+j*Skip,PrevBars+50,HighPrice);
      CopyLow(_Symbol,_Period,1+j*Skip,PrevBars+50,LowPrice);
      
      CenterPoint = ClosePrice[PrevBars-1];
      FileLine = "";
      for(int i=0;i<PrevBars;i++){
         LowPrice[i] = LowPrice[i]-CenterPoint;
         HighPrice[i] = HighPrice[i]-CenterPoint;
         OpenPrice[i] = OpenPrice[i]-CenterPoint;
         ClosePrice[i] = ClosePrice[i]-CenterPoint;
         FileLine = FileLine+DoubleToString(LowPrice[i])+",";
         FileLine = FileLine+DoubleToString(HighPrice[i])+",";
         FileLine = FileLine+DoubleToString(OpenPrice[i])+",";
         FileLine = FileLine+DoubleToString(ClosePrice[i])+",";
         FileLine = FileLine+DoubleToString(VolumeBuffer[i],9)+",";
      }
      FileLine = FileLine+DoubleToString(ClosePrice[PrevBars+4]-CenterPoint)+",";
      FileLine = FileLine+DoubleToString(ClosePrice[PrevBars+9]-CenterPoint)+",";
      FileLine = FileLine+DoubleToString(ClosePrice[PrevBars+24]-CenterPoint)+",";
      FileLine = FileLine+DoubleToString(ClosePrice[PrevBars+49]-CenterPoint);
      FileWrite(FileHandle,FileLine);
   }
   
}
//+------------------------------------------------------------------+
