//+------------------------------------------------------------------+
//|                                                   TestZigzag.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
//---
   int ZZHandle = iCustom(_Symbol,_Period,"Examples/ZigZag",30,5,3);
   double ZZ[3];
   double ZZBuffer[];
   CopyBuffer(ZZHandle,0,0,300,ZZBuffer);
   ArraySetAsSeries(ZZBuffer,true);
   int a = 0;
   for(int i=0;i<300;i++){
      if(ZZBuffer[i] > 0){
         ZZ[a] = ZZBuffer[i];
         a++;
      }
      if(a > 2) break;
   }
   Alert(ZZ[0]+" , "+ZZ[1]+" , "+ZZ[2]);
   //Alert(DoubleToString(ZZBuffer[1])+","+DoubleToString(ZZBuffer[66])+","+DoubleToString(ZZBuffer[67])+","+DoubleToString(ZZBuffer[68]));
   
   
}
//+------------------------------------------------------------------+
