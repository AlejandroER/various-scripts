//+------------------------------------------------------------------+
//|                                                        OBVMA.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot OBV
#property indicator_label1  "OBV"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrTurquoise
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot MA
#property indicator_label2  "MA"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrMintCream
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
#include <MovingAverages.mqh>
//--- input parameters
input int      MAPeriod=14;
input ENUM_MA_METHOD MAMethod=MODE_SMMA;
input ENUM_APPLIED_VOLUME OBVVolType=VOLUME_TICK;
//--- indicator buffers
double         OBVBuffer[];
double         MABuffer[];
int            OBVHandle=0;
int            MAHandle=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit(){
//--- indicator buffers mapping
   SetIndexBuffer(0,OBVBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,MABuffer,INDICATOR_DATA);
   OBVHandle = iOBV(_Symbol,_Period,OBVVolType);
   MAHandle = iMA(_Symbol,_Period,MAPeriod,0,MODE_SMMA,OBVHandle);
   ArraySetAsSeries(OBVBuffer,true);
   ArraySetAsSeries(MABuffer,true);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const int begin,
                const double &price[]){
//---
   
   CopyBuffer(OBVHandle,0,0,rates_total,OBVBuffer);
   CopyBuffer(MAHandle,0,0,rates_total,MABuffer);
   
   
   
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
