//+------------------------------------------------------------------+
//|                                                 TradingTest2.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
CTrade trade;

void OnStart(){
   ulong ticket;
   MqlTradeRequest RequestData={0};
   MqlTradeResult ResultData={0};
   //trade.BuyStop(0.01,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+10*_Point,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SymbolInfoDouble(_Symbol,SYMBOL_ASK)+20*_Point,ORDER_TIME_GTC,0,"Testing Closing PendingOrders");
   //trade.Buy(0.01,_Symbol,0,SymbolInfoDouble(_Symbol,SYMBOL_BID)-10*_Point,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+10*_Point,"Testing Closing PendingOrders");
   trade.OrderOpen(_Symbol,ORDER_TYPE_SELL_STOP,0.01,0,SymbolInfoDouble(_Symbol,SYMBOL_BID)-1*_Point,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+10*_Point,SymbolInfoDouble(_Symbol,SYMBOL_BID)-20*_Point,0,0,"TestOrder1");
   ticket = trade.ResultOrder();
   //BSSL = trade.RequestSL();
   //trade.Request(RequestData);
   
   Alert("Ticket: ",ticket);
   
   Sleep(15000);
   
   Alert("Total: ",OrdersTotal(),";","Ticket0: ",OrderGetTicket(0),";","Ticket1: ",OrderGetTicket(1),";","Ticket2: ",OrderGetTicket(2),";","Ticket3: ",OrderGetTicket(3));
   Alert("Total: ",PositionsTotal(),";","Ticket0: ",PositionGetTicket(0),";","Ticket1: ",PositionGetTicket(1),";","Ticket2: ",PositionGetTicket(2),";","Ticket3: ",PositionGetTicket(3));
   
   OrderSelect(ticket);
   
   
   //trade.PositionOpen(_Symbol,ORDER_TYPE_SELL,0.01,SymbolInfoDouble(_Symbol,SYMBOL_BID),0,0,"");
   //trade.PositionOpen(_Symbol,ORDER_TYPE_BUY,0.01,SymbolInfoDouble(_Symbol,SYMBOL_ASK),0,0,"");
   //trade.PositionOpen(_Symbol,ORDER_TYPE_SELL,0.01,SymbolInfoDouble(_Symbol,SYMBOL_BID),0,0,"");
   
   Alert("Order Comment: ",OrderGetString(ORDER_COMMENT));
   PositionSelectByTicket(ticket);
   Alert("Position Comment: ",PositionGetString(POSITION_COMMENT));
   Alert("Position ID: ",PositionGetInteger(POSITION_IDENTIFIER));
   Alert("Position Ticket: ",PositionGetInteger(POSITION_TICKET));
   
   
   trade.OrderDelete(ticket);
   
   Sleep(5000);
   
   Alert("Total: ",OrdersTotal(),";","Ticket0: ",OrderGetTicket(0),";","Ticket1: ",OrderGetTicket(1),";","Ticket2: ",OrderGetTicket(2),";","Ticket3: ",OrderGetTicket(3));
   Alert("Total: ",PositionsTotal(),";","Ticket0: ",PositionGetTicket(0),";","Ticket1: ",PositionGetTicket(1),";","Ticket2: ",PositionGetTicket(2),";","Ticket3: ",PositionGetTicket(3));
   
   trade.PositionClose(ticket);
   
   Sleep(5000);
   
   Alert("Total: ",OrdersTotal(),";","Ticket0: ",OrderGetTicket(0),";","Ticket1: ",OrderGetTicket(1),";","Ticket2: ",OrderGetTicket(2),";","Ticket3: ",OrderGetTicket(3));
   Alert("Total: ",PositionsTotal(),";","Ticket0: ",PositionGetTicket(0),";","Ticket1: ",PositionGetTicket(1),";","Ticket2: ",PositionGetTicket(2),";","Ticket3: ",PositionGetTicket(3));
   
   
   //OrderSelect(ticket);
   
//   Alert("Position: ",OrderGetInteger(ORDER_POSITION_ID));
//   Alert("Order: ",OrderGetInteger(ORDER_TICKET));
//   trade.OrderDelete(ticket);
//   Alert("Position: ",OrderGetInteger(ORDER_POSITION_ID));
//   Alert("Order: ",OrderGetInteger(ORDER_TICKET));
//   
//   trade.OrderOpen(_Symbol,ORDER_TYPE_SELL_STOP,0.01,0,SymbolInfoDouble(_Symbol,SYMBOL_BID)-1*_Point,SymbolInfoDouble(_Symbol,SYMBOL_ASK)+10*_Point,SymbolInfoDouble(_Symbol,SYMBOL_BID)-20*_Point);
//   ticket = trade.ResultOrder();
//   //OrderSelect(ticket);
//   
//   Sleep(15000);
//   Alert("Position: ",trade.RequestPosition());
//   Alert("Deal: ",trade.ResultDeal());
//   Alert("Order: ",trade.ResultOrder());
//   OrderSelect(ticket);
//   //Alert("Position: ",OrderGetInteger(ORDER_POSITION_ID));
//   //Alert("Order: ",OrderGetInteger(ORDER_TICKET));
//   trade.OrderDelete(ticket);
//   Sleep(2000);
//   Alert("Position: ",trade.RequestPosition());
//   Alert("Deal: ",trade.ResultDeal());
//   Alert("Order: ",trade.ResultOrder());
//   //Alert("Position: ",OrderGetInteger(ORDER_POSITION_ID));
//   //Alert("Order: ",OrderGetInteger(ORDER_TICKET));
   
   //Alert("Buy: ",ORDER_TYPE_BUY);
   //Alert("Sell: ",ORDER_TYPE_SELL);
   //Alert("BuyStop: ",ORDER_TYPE_BUY_STOP);
   //Alert("SellStop: ",ORDER_TYPE_SELL_STOP);
   //Alert("BuyLimit: ",ORDER_TYPE_BUY_LIMIT);
   //Alert("SellLimit: ",ORDER_TYPE_SELL_LIMIT);
   //Sleep(15000);
   //trade.Request(RequestData);
   //Alert("DataPos: ",RequestData.position);
   //Alert("SL: ",BSSL);
   //Alert("Ticket: ",ticket);
   //Alert("Type: ",RequestData.type);
   //Alert("Type: ",trade.RequestType());
   //Alert("Position: ",trade.RequestPosition());
   //Alert("Deal: ",trade.ResultDeal());
   //Alert("Order: ",trade.ResultOrder());
   //trade.OrderDelete(ticket);
   ////trade.PositionClose(ticket);
   //Sleep(5000);
   ////Alert("Type: ",trade.RequestTypeDescription());
   //Alert("Position: ",trade.RequestPosition());
   //Alert("Type: ",trade.RequestType());
   //Alert("Deal: ",trade.ResultDeal());
   //Alert("Order: ",trade.ResultOrder());
}
//+------------------------------------------------------------------+
