//+------------------------------------------------------------------+
//|                                                    TestClose.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   
   datetime LastBreak=StringToTime("2017.01.03 [00:00]");
   datetime PrevBreak=StringToTime("2017.01.03 [01:35]");
   int TheBars;
   TheBars = Bars(_Symbol,_Period,LastBreak,PrevBreak);
   
   Alert(TheBars);
}
//+------------------------------------------------------------------+