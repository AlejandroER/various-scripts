//+------------------------------------------------------------------+
//|                                             TripleMABreakout.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
//--- input parameters
input int      BarNums=10;
input double   Slope=0.2;
input int      MALong=200;
input int      MAMedium=100;
input int      MAShort=50;
input double   AvailableCash=300;

CTrade trade;
datetime LastBreak=StringToTime("2015.01.01 [00:00]");
int MALongHandle;
int MAMediumHandle;
int MAShortHandle;
ulong OrderTicket=0;
int OrderType=0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   
   if(BarNums<3){
      Alert("BarNums has to be greater than 3.");
      return(INIT_PARAMETERS_INCORRECT);
   }
   
   string EAComment;
   EAComment = "TripleMABreakout: "+_Symbol+","+string(_Period);
   
   ulong MagicNumber;
   MagicNumber = 111110000+_Period;
   trade.SetExpertMagicNumber(MagicNumber);
   ulong TestOrderTicket;
   int AllPositions;
   AllPositions = PositionsTotal();
   for(int i=0;i<AllPositions;i++){
      TestOrderTicket = PositionGetTicket(i);
      if(!PositionSelectByTicket(TestOrderTicket)){
         Sleep(1000);
         if(!PositionSelectByTicket(TestOrderTicket)){
            TestOrderTicket = 0;
            Sleep(1000);
            return(INIT_FAILED);
         }
      }
      // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
      //and if there is an open position from a previous session we grab that ticket.
      if(PositionGetString(POSITION_COMMENT) == EAComment){
         OrderTicket = TestOrderTicket;
         // We now get what type of operation opened that position.
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY){
            OrderType = 1;
         } else if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL){
            OrderType = -1;
         }
         break;
      }
   }
   
   MALongHandle = iMA(_Symbol,_Period,MALong,0,MODE_SMMA,PRICE_CLOSE);
   MAMediumHandle = iMA(_Symbol,_Period,MAMedium,0,MODE_SMMA,PRICE_CLOSE);
   MAShortHandle = iMA(_Symbol,_Period,MAShort,0,MODE_SMMA,PRICE_CLOSE);
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
   
   
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   double MALongBuffer[],MAMediumBuffer[],MAShortBuffer[],MAShortReg[2],MALongReg[2],MAMediumReg[2],PriceBuffer[];
   double RiskCash=(AvailableCash*0.01);
   CopyBuffer(MALongHandle,0,1,BarNums,MALongBuffer);
   CopyBuffer(MAMediumHandle,0,1,BarNums,MAMediumBuffer);
   CopyBuffer(MAShortHandle,0,1,BarNums,MAShortBuffer);
   CopyClose(_Symbol,_Period,1,2,PriceBuffer);
   string EAComment;
   int LastBar = BarNums-1;
   EAComment = "TripleMABreakout: "+_Symbol+","+string(_Period);
   double Volume=0.01;
   int AllPositions;
   AllPositions = PositionsTotal();
   if(AllPositions == 0 && (OrderType != 0 || OrderTicket != 0)){
      OrderTicket = 0;
      OrderType = 0;
   }
   ulong TestOrderTicket=0;
   if(OrderType != 0 && OrderTicket == 0){
      for(int i=0;i<AllPositions;i++){
         TestOrderTicket = PositionGetTicket(i);
         if(!PositionSelectByTicket(TestOrderTicket)){
            Sleep(1000);
            if(!PositionSelectByTicket(TestOrderTicket)){
               TestOrderTicket = 0;
               Sleep(1000);
               return;
            }
         }
         // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
         if(PositionGetString(POSITION_COMMENT) == EAComment){
            OrderTicket = TestOrderTicket;
            break;
         }
      }
      if(OrderTicket == 0) OrderType = 0;
   }
   
   if(OrderTicket>0){         // We check if we have an open position by this EA.
      bool TicketExists = false;
      for(int i=0;i<AllPositions;i++){
         TestOrderTicket = PositionGetTicket(i);
         if(!PositionSelectByTicket(TestOrderTicket)){
            Sleep(1000);
            if(!PositionSelectByTicket(TestOrderTicket)){
               TestOrderTicket = 0;
               Sleep(1000);
            }
         }
         if(PositionGetString(POSITION_COMMENT) == EAComment){
            TicketExists = true;
            break;
         }
      }
      if(!TicketExists){   // If no open position matches a position opened by this EA then we set OrederTicket and OrderType to 0.
         OrderTicket = 0;
         OrderType = 0;
         return;
      }
      //------------------------------------------------------------------------------------------------------------------------------------------------
      //------------This Block of code checks if the close price breaks the Short MA and, if it does, closes the position.------------------------------
      if(OrderType>0 && PriceBuffer[1]<MAShortBuffer[LastBar]){
         if(!trade.PositionClose(OrderTicket)){
            Sleep(1000);
            if(!trade.PositionClose(OrderTicket)){
               Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               Sleep(1000);
               return;
            }
         }
         if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
            Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
            return;
         }
      } else if(OrderType<0 && PriceBuffer[1]>MAShortBuffer[LastBar]) {
         if(!trade.PositionClose(OrderTicket)){
            Sleep(1000);
            if(!trade.PositionClose(OrderTicket)){
               Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               Sleep(1000);
               return;
            }
         }
         if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
            Alert("Error Closing Ticket "+string(OrderTicket)+" with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
            return;
         }
      }
      //--------------------------------------------------------------------------------------------------------------------------------------------------
   } else if(MAShortBuffer[LastBar]>MAMediumBuffer[LastBar] && MAMediumBuffer[LastBar]>MALongBuffer[LastBar]){
      if(PriceBuffer[0]<MAShortBuffer[LastBar-1] && PriceBuffer[1]>MAShortBuffer[LastBar] && Bars(_Symbol,_Period,LastBreak,TimeLocal())>1){
         LastBreak = TimeLocal();
         MALinReg(MALongBuffer,MALongReg);
         MALinReg(MAMediumBuffer,MAMediumReg);
         MALinReg(MAShortBuffer,MAShortReg);
         //if(MALongReg[1]>Slope){
         //if(MALongReg[1]>Slope && MAMediumReg[1]>Slope){
         if(MALongReg[1]>Slope && MAMediumReg[1]>Slope && MAShortReg[1]>Slope){
            double SL,TestVolume,TP;
            SL = MAMediumBuffer[LastBar] - 2*(SymbolInfoDouble(_Symbol,SYMBOL_ASK) - SymbolInfoDouble(_Symbol,SYMBOL_BID));
            
            TestVolume = SymbolInfoDouble(_Symbol,SYMBOL_ASK)-SL;
            TestVolume = TestVolume*MathPow(10,_Digits);
            TestVolume = RiskCash/TestVolume;
            TestVolume = floor(TestVolume*100)/100;
            if(TestVolume>Volume){
               Volume = TestVolume;
            }
            
            TP = SymbolInfoDouble(_Symbol,SYMBOL_ASK)+1200*_Point;
            if(!trade.Buy(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment)){
               Sleep(1000);
               if(!trade.Buy(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment)){
                  Alert("Error Opening Buy Position with Error Code "+string(trade.ResultRetcode()));
                  Sleep(1000);
                  return;
               }
            }
            if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
               Alert("Error Opening Sell Position with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               ExpertRemove();
            }
            OrderType = 1;
            OrderTicket = trade.RequestPosition();
            Alert("Ticket: "+string(OrderTicket));
         }
      }
   } else if(MAShortBuffer[LastBar]<MAMediumBuffer[LastBar] && MAMediumBuffer[LastBar]<MALongBuffer[LastBar]){
      if(PriceBuffer[0]>MAShortBuffer[LastBar-1] && PriceBuffer[1]<MAShortBuffer[LastBar] && Bars(_Symbol,_Period,LastBreak,TimeLocal())>1){
         LastBreak = TimeLocal();
         MALinReg(MALongBuffer,MALongReg);
         MALinReg(MAMediumBuffer,MAMediumReg);
         MALinReg(MAShortBuffer,MAShortReg);
         //if(MALongReg[1]<(-Slope)){
         //if(MALongReg[1]<(-Slope) && MAMediumReg[1]<(-Slope)){
         if(MALongReg[1]<(-Slope) && MAMediumReg[1]<(-Slope) && MAShortReg[1]<(-Slope)){
            double SL,TestVolume,TP;
            SL = MAMediumBuffer[LastBar] + 2*(SymbolInfoDouble(_Symbol,SYMBOL_ASK) - SymbolInfoDouble(_Symbol,SYMBOL_BID));
            
            TestVolume = SL-SymbolInfoDouble(_Symbol,SYMBOL_BID);
            TestVolume = TestVolume*MathPow(10,_Digits);
            TestVolume = RiskCash/TestVolume;
            TestVolume = floor(TestVolume*100)/100;
            if(TestVolume>Volume){
               Volume = TestVolume;
            }
            
            TP = SymbolInfoDouble(_Symbol,SYMBOL_BID)-1200*_Point;
            if(!trade.Sell(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment)){
               Sleep(1000);
               if(!trade.Sell(Volume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment)){
                  Alert("Error Opening Sell Position with Error Code "+string(trade.ResultRetcode()));
                  Sleep(1000);
                  return;
               }
            }
            if(trade.ResultRetcode() != TRADE_RETCODE_DONE){
               Alert("Error Opening Sell Position with Error Code "+string(trade.ResultRetcode())+": "+trade.ResultRetcodeDescription());
               ExpertRemove();
            }
            OrderType = -1;
            OrderTicket = trade.RequestPosition();
            Alert("Ticket: "+string(OrderTicket));
         }
      }
   }
   
      
}
//+------------------------------------------------------------------+
//| Linear Regression of MA values                                   |
//+------------------------------------------------------------------+
void MALinReg(double &MAValues[],double &RegCoef[]){
   int ValNum;
   ValNum = ArraySize(MAValues);
   
   double Entries[];
   ArrayResize(Entries,ValNum,0);
   for(int i=0;i<ValNum;i++){
      Entries[i] = i*0.4;
   }
   double Coef1,Coef0;
   Coef1 = Covar2(MAValues,Entries)/Covar2(Entries,Entries);
   Coef0 = Mean(MAValues) - Coef1*Mean(Entries);
   
   RegCoef[0] = Coef0;
   RegCoef[1] = Coef1;
   //Alert("Coef0: "+string(RegCoef[0])+"; Coef1: "+string(RegCoef[1])+".");
   
}
//+------------------------------------------------------------------+
//| Covariance Calculation of two Arrays of same size                |
//+------------------------------------------------------------------+
double Covar2(double &Vector1[],double &Vector2[]){
   int Size;
   Size = ArraySize(Vector1);
   if(Size!=ArraySize(Vector2)){
      Alert("Arrays must be of same size for Covariance calculations.");
      return(0);
   }
   double Mean1,Mean2;
   Mean1 = Mean(Vector1);
   Mean2 = Mean(Vector2);
   //Alert("Covar Size: "+string(Size));
   double Covar=0;
   for(int i=0;i<Size;i++){
      Covar = Covar + (Vector1[i]-Mean1)*(Vector2[i]-Mean2);
   }
   //    Covar = Covar/(double(Size)-1);
   return(Covar);
}
//+------------------------------------------------------------------+
//| Covariance Calculation of two Arrays of same size                |
//+------------------------------------------------------------------+
double Mean(double &Vector[]){
   int Size;
   Size = ArraySize(Vector);
   if(Size==0){
      Alert("Array for Mean calculation has no entries.");
      return(0);
   }
   //Alert("Mean Size: "+string(Size));
   double Mean=0;
   for(int i=0;i<Size;i++){
      Mean += Vector[i];
   }
   Mean = Mean/double(Size);
   return(Mean);
   
}