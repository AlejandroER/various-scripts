//+------------------------------------------------------------------+
//|                                               ExtractHammers.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property version   "1.00"
#property script_show_inputs
input string StartDate = "2016.01.01 [00:00]";
input int TakeBars = 5;
input double CheckHammer = 0.3;
datetime Start = StringToTime(StartDate);
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   int Handle=-1,i=0,OutputArray[];
   int TestBar = (TakeBars-1)/2;
   string VarNames[];
   double HighPrice[],LowPrice[],OpenPrice[],ClosePrice[];
   int ArraysLength = 6 + (TestBar - 1)*3;
   ArrayResize(HighPrice,TakeBars);
   ArrayResize(LowPrice,TakeBars);
   ArrayResize(OpenPrice,TakeBars);
   ArrayResize(ClosePrice,TakeBars);
   ArrayResize(OutputArray,ArraysLength);
   ArrayResize(VarNames,ArraysLength);
   int NumBars;
   MqlDateTime BarTime;
   NumBars = Bars(_Symbol,_Period,Start,TimeLocal());
   Handle = FileOpen(("Hammers"+string(_Symbol)+string(_Period)+".csv"),FILE_READ|FILE_WRITE|FILE_CSV|FILE_ANSI);
   VarNames[0] = "UpDown";
   VarNames[1] = "CloseToOpen";
   VarNames[2] = "CloseToHandle";
   VarNames[3] = "Bar1More";
   VarNames[4] = "Bar1Below";
   VarNames[5] = "Prev1Open";
   int Element=6,CurBar=2;
   for(i=0;i<TestBar-1;i++){
      VarNames[Element] = "Bar"+string(CurBar+i)+"More";
      Element++;
      VarNames[Element] = "Bar"+string(CurBar+i)+"Below";
      Element++;
      
   }
   CurBar=2;
   for(i=0;i<TestBar-1;i++){
      VarNames[Element] = "Prev"+string(CurBar+i)+"Open";
      Element++;
   }
   
   FileWriteArrayCSV(VarNames,Handle,ArraysLength);
   
   for(int j=1;j<NumBars;j++){
      CopyHigh(_Symbol,_Period,j,TakeBars,HighPrice);
      CopyLow(_Symbol,_Period,j,TakeBars,LowPrice);
      CopyOpen(_Symbol,_Period,j,TakeBars,OpenPrice);
      CopyClose(_Symbol,_Period,j,TakeBars,ClosePrice);
      
      TimeToStruct(iTime(_Symbol,_Period,j),BarTime);
      
      bool EURUSDTime;
      EURUSDTime = BarTime.hour>10 && BarTime.hour<22;
      
      if(MathAbs(HighPrice[TestBar]-ClosePrice[TestBar]) < CheckHammer*(ClosePrice[TestBar]-OpenPrice[TestBar]) && (OpenPrice[TestBar]-LowPrice[TestBar])>(ClosePrice[TestBar]-OpenPrice[TestBar]) && EURUSDTime){
         OutputArray[0] = 1;
         OutputArray[1] = int((ClosePrice[TestBar] - OpenPrice[TestBar])*100000);
         OutputArray[2] = int((ClosePrice[TestBar] - LowPrice[TestBar])*100000);
         OutputArray[3] = int((HighPrice[TestBar+1] - ClosePrice[TestBar])*100000);
         OutputArray[4] = int((LowPrice[TestBar+1] - LowPrice[TestBar])*100000);
         OutputArray[5] = int((OpenPrice[TestBar-1] - ClosePrice[TestBar])*100000);
         
         Element=6;
         CurBar=2;
         for(i=0;i<TestBar-1;i++){
            OutputArray[Element] = int((HighPrice[TestBar+CurBar+i] - ClosePrice[TestBar])*100000);
            Element++;
            OutputArray[Element] = int((LowPrice[TestBar+CurBar+i] - LowPrice[TestBar])*100000);
            Element++;
      
         }
         CurBar=2;
         for(i=0;i<TestBar-1;i++){
            OutputArray[Element] = int((OpenPrice[TestBar-CurBar-i] - ClosePrice[TestBar])*100000);
            Element++;
         }
         FileWriteArrayCSV(OutputArray,Handle,ArraysLength);
         Print(TimeToString(iTime(_Symbol,_Period,j),TIME_DATE));
         continue;
      }
      
      if(MathAbs(LowPrice[TestBar]-ClosePrice[TestBar]) < CheckHammer*(OpenPrice[TestBar]-ClosePrice[TestBar]) && (HighPrice[TestBar] - OpenPrice[TestBar])>(OpenPrice[TestBar] - ClosePrice[TestBar]) && EURUSDTime){
         OutputArray[0] = -1;
         OutputArray[1] = int((OpenPrice[TestBar] - ClosePrice[TestBar])*100000);
         OutputArray[2] = int((HighPrice[TestBar] - ClosePrice[TestBar])*100000);
         OutputArray[3] = int((ClosePrice[TestBar] - LowPrice[TestBar+1])*100000);
         OutputArray[4] = int((HighPrice[TestBar] - HighPrice[TestBar+1])*100000);
         OutputArray[5] = int((ClosePrice[TestBar] - OpenPrice[TestBar-1])*100000);
         
         Element=6;
         CurBar=2;
         for(i=0;i<TestBar-1;i++){
            OutputArray[Element] = int((ClosePrice[TestBar] - LowPrice[TestBar+CurBar+i])*100000);
            Element++;
            OutputArray[Element] = int((HighPrice[TestBar] - HighPrice[TestBar+CurBar+i])*100000);
            Element++;
      
         }
         CurBar=2;
         for(i=0;i<TestBar-1;i++){
            OutputArray[Element] = int((ClosePrice[TestBar] - OpenPrice[TestBar-CurBar-i])*100000);
            Element++;
         }
         Print(TimeToString(iTime(_Symbol,_Period,j),TIME_DATE));
         FileWriteArrayCSV(OutputArray,Handle,ArraysLength);
      }
      
   }
   
   
}
//+------------------------------------------------------------------+
// This function writes a double array to a file with comas separating the elements of the array and a /r/n at the end of the array
// If FileHandle = 0 it creates a new file in the Files directory named ArrayCSV.csv.
void FileWriteArrayCSV(double &DoubleArray[],int FileHandle=0,int ArrayLength = -1){
   if(FileHandle == 0){
      FileHandle = FileOpen("ArrayCSV.csv",FILE_READ|FILE_WRITE|FILE_CSV|FILE_ANSI);
   }
   if(ArrayLength == -1){
      ArrayLength = ArraySize(DoubleArray);
   }
   
   for(int i=0;i<ArrayLength;i++){
      FileWriteString(FileHandle,DoubleToString(DoubleArray[i],10));
      if(i < (ArrayLength-1)){
         FileWriteString(FileHandle,",");
      } else {
         FileWriteString(FileHandle,"\r\n");
      }
      
      
   }
}
//+------------------------------------------------------------------+
// This function writes a integer array to a file with comas separating the elements of the array and a /r/n at the end of the array
// If FileHandle = 0 it creates a new file in the Files directory named ArrayCSV.csv.
void FileWriteArrayCSV(int &IntArray[],int FileHandle=0,int ArrayLength = -1){
   if(FileHandle == 0){
      FileHandle = FileOpen("ArrayCSV.csv",FILE_READ|FILE_WRITE|FILE_CSV|FILE_ANSI);
   }
   if(ArrayLength == -1){
      ArrayLength = ArraySize(IntArray);
   }
   
   for(int i=0;i<ArrayLength;i++){
      FileWriteString(FileHandle,IntegerToString(IntArray[i]));
      if(i < (ArrayLength-1)){
         FileWriteString(FileHandle,",");
      } else {
         FileWriteString(FileHandle,"\r\n");
      }
      
      
   }
}
//+------------------------------------------------------------------+
// This function writes a strin array to a file with comas separating the elements of the array and a /r/n at the end of the array
// If FileHandle = 0 it creates a new file in the Files directory named ArrayCSV.csv.
void FileWriteArrayCSV(string &StringArray[],int FileHandle=0,int ArrayLength = -1){
   if(FileHandle == 0){
      FileHandle = FileOpen("ArrayCSV.csv",FILE_READ|FILE_WRITE|FILE_CSV|FILE_ANSI);
   }
   if(ArrayLength == -1){
      ArrayLength = ArraySize(StringArray);
   }
   
   for(int i=0;i<ArrayLength;i++){
      FileWriteString(FileHandle,StringArray[i]);
      if(i < (ArrayLength-1)){
         FileWriteString(FileHandle,",");
      } else {
         FileWriteString(FileHandle,"\r\n");
      }
      
      
   }
}