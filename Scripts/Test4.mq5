//+------------------------------------------------------------------+
//|                                                        Test4.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+

int barNum = 60;                 // The number of bars the EA will look back to check if the market is ranging.
int FractSep = 3;                // How far apart must two fractals be to be taken into consideration for IsRanging.
double RangeBorderBuffer = 0.2;  // Percentile of the Range whithin which must be the two highest and two lowest fractals.
double StartVolume = 0.1;        // Starting Volume of operation for Money Management.
double StartCapital = 300;       // Starting Capital available to the EA.
double EAProfit = 0;             // The amount of money the EA has earned over it's life. Used for MoneyManagement.
int ExtraSLPoints = 0;           // How many points are added to the Border + Spread + 1 base StopLoss.
double PercRangePrice = 0.2;     // Percentile of the Range where the entry into the market is to be set.
double PercRangeTP = 0.6;        // Percentile of the Range where TakeProfit is to be set.
bool IsInRange = false;                // Have we already determined that the market is ranging.
double RangeHeight,Lowest,Highest;


void OnStart(){
   
   double HighPrices[],LowPrices[];
   ArraySetAsSeries(HighPrices,true);
   ArraySetAsSeries(LowPrices,true);
   ArrayResize(HighPrices,barNum);
   ArrayResize(LowPrices,barNum);
   
   if(CopyHigh(_Symbol,_Period,2,barNum,HighPrices)==-1){
      Sleep(2000);
      CopyHigh(_Symbol,_Period,2,barNum,HighPrices);
   }
   if(CopyLow(_Symbol,_Period,2,barNum,LowPrices)==-1){
      Sleep(2000);
      CopyLow(_Symbol,_Period,2,barNum,LowPrices);
   }
   
   IsInRange = IsRanging(HighPrices,LowPrices); // ,RangeHeight,Highest,Lowest,barNum,FractSep,RangeBorderBuffer);
         if(IsInRange){
            Print("Price is in Range");
         }
   
}
//+------------------------------------------------------------------+


// We check if the center bar of the sequence is a Bearish Fractal for the whole sequence.
bool IsUpFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] > Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bullish Fractal for the whole sequence.
bool IsDownFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] < Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// In this function we check if the Market is ranging or not.
bool IsRanging(double &HighPrice[],double &LowPrice[])   // ,double &RangeHeight,double &Highest, double &Lowest, int barNum,int FractSep, double RangeBorderBuffer)
{
   double UpFractals[],DownFractals[],TestBars[];
   int FractSize = 5;                              // This is how many bars it takes to form a fractal.
   // ArrayResize(TestBars,FractSize); 
   int UpFBarIndex[],DownFBarIndex[],Down = 0,Up = 0,BestHigh1 = 0,BestLow1 = 0,BestHigh2 = 0,BestLow2 = 0;
   
   ArrayResize(UpFractals,barNum);
   ArrayResize(DownFractals,barNum);
   ArrayResize(UpFBarIndex,barNum);
   ArrayResize(DownFBarIndex,barNum);
   // We populate two vectors for Up and Down fractals, one of the values at fractal and another of the candle number at which the fractal appears.
   for(int i=2;i<barNum-2;i++){
      ArrayCopy(TestBars,HighPrice,0,i-2,FractSize);
      // TestBars[0] = HighPrice[i-2]; TestBars[1] = HighPrice[i-1]; TestBars[2] = HighPrice[i]; TestBars[3] = HighPrice[i+1]; TestBars[4] = HighPrice[i+2];
      if(IsUpFractal(TestBars,FractSize)){
         UpFractals[Up] = HighPrice[i];
         UpFBarIndex[Up] = i;
         Up++;
         // Here we find the two highest of all the fractals that are more than FractSep appart.
         if(HighPrice[i] > HighPrice[BestHigh1]){
            if(i-BestHigh1 > FractSep){
               BestHigh2 = BestHigh1;
            }
            BestHigh1 = i;
         }
      }
      ArrayCopy(TestBars,LowPrice,0,i-2,FractSize);
      // TestBars[0] = LowPrice[i-2]; TestBars[1] = LowPrice[i-1]; TestBars[2] = LowPrice[i]; TestBars[3] = LowPrice[i+1]; TestBars[4] = LowPrice[i+2];
      if(IsDownFractal(TestBars,FractSize)){
      Alert("Bar:",i,"----",LowPrice[i]);
         DownFractals[Down] = LowPrice[i];
         DownFBarIndex[Down] = i;
         Down++;
         // Here we find the two lowest of all the fractals that are more than FractSep appart.
         if(LowPrice[i] < LowPrice[BestLow1]){
            if(i-BestLow1 > FractSep){
               BestLow2 = BestLow1;
            }
            BestLow1 = i;
         }
      }
   }
   //Alert("UP: ",ArraySize(UpFractals)," --- ","DOWN: ",ArraySize(DownFractals));
   //Alert(BestHigh1);
   //Alert(BestLow1);
   //Alert(BestHigh2);
   //Alert(BestLow2);
   //Alert(LowPrice[0]);
   if(BestHigh1 == 0 || BestLow1 == 0 || BestHigh2 == 0 || BestLow2 == 0){
      return(false);
   }
   
   RangeHeight = HighPrice[BestHigh1] - LowPrice[BestLow1];
   // Alert(RangeHeight*RangeBorderBuffer);
   // Alert(HighPrice[BestHigh1]-HighPrice[BestHigh2]);
   // Alert(LowPrice[BestLow2]-LowPrice[BestLow1]);
   if(HighPrice[BestHigh1]-HighPrice[BestHigh2]<RangeHeight*RangeBorderBuffer && LowPrice[BestLow2]-LowPrice[BestLow1]<RangeHeight*RangeBorderBuffer){
      Highest = HighPrice[BestHigh1];
      Lowest = LowPrice[BestLow1];
      // Alert(Highest);
      return(true);
   } else {
      return(false);
   }
}
//+------------------------------------------------------------------+