//+------------------------------------------------------------------+
//|                                              Reversal Arrows.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot UpArrows
#property indicator_label1  "UpArrows"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrLime
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot DownArrows
#property indicator_label2  "DownArrows"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buffers
double         UpArrowsBuffer[];
double         DownArrowsBuffer[];
//--- Input parameters
input int FastEMA = 12;
input int SlowEMA = 26;
input int MACDSMA = 9;
input int StochK = 70;
input int StockD = 10;
input int Slowing = 10;
//--- Global parameters
int MACDHandle=0;
int StocHandle=0;
double MACDBuffer[],StochBufferMain[1],StochBufferSignal[1];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,UpArrowsBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,DownArrowsBuffer,INDICATOR_DATA);
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
   PlotIndexSetInteger(0,PLOT_ARROW,221);
   PlotIndexSetInteger(1,PLOT_ARROW,222);
   //PlotIndexSetInteger(0,PLOT_ARROW,233);
   //PlotIndexSetInteger(1,PLOT_ARROW,234);
   PlotIndexSetInteger(0,PLOT_ARROW_SHIFT,25);
   PlotIndexSetInteger(1,PLOT_ARROW_SHIFT,-25);
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,0.0);
   
   MACDHandle = iMACD(_Symbol,_Period,FastEMA,SlowEMA,MACDSMA,PRICE_CLOSE);
   StocHandle = iStochastic(_Symbol,_Period,StochK,StockD,Slowing,MODE_SMA,STO_LOWHIGH);
   ArraySetAsSeries(MACDBuffer,true);
   ArraySetAsSeries(UpArrowsBuffer,true);
   ArraySetAsSeries(DownArrowsBuffer,true);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int first=0;
   bool up=true,down=true;
   if(prev_calculated == 0){  
      first = rates_total-1;
   } else {
      first = rates_total - prev_calculated - 1;
   }
   ArraySetAsSeries(low,true);
   ArraySetAsSeries(high,true);
   for(int i=first;i>0;i--){
      up = true;
      down = true;
      CopyBuffer(MACDHandle,0,i,2,MACDBuffer);
      CopyBuffer(StocHandle,0,i,1,StochBufferMain);
      CopyBuffer(StocHandle,1,i,1,StochBufferSignal);
      //if(i == 34){
      //   Alert(MACDBuffer[1]);
      //   Alert(MACDBuffer[0]);
      //   Alert(StochBufferMain[0]);
      //   Alert(StochBufferSignal[0]);
      //   Alert(high[i]);
      //   Alert(low[i]);
      //}
      up = up&&(MACDBuffer[1]<0&&MACDBuffer[0]>0)&&(StochBufferMain[0]<30)&&(StochBufferSignal[0]<30);
      down = down&&(MACDBuffer[1]>0&&MACDBuffer[0]<0)&&(StochBufferMain[0]>70)&&(StochBufferSignal[0]>70);
      //if(i == 34) Alert(down);
      if(up){
         UpArrowsBuffer[i] = low[i];
         DownArrowsBuffer[i] = 0;
      } else if(down) {
         DownArrowsBuffer[i] = high[i];
      //   Alert(high[i]);
         UpArrowsBuffer[i] = 0;
      } else {
         UpArrowsBuffer[i] = 0.0;
         DownArrowsBuffer[i] = 0.0;
      }
   }
   ChartRedraw(0);
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
