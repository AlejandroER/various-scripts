//+------------------------------------------------------------------+
//|                                                MondayPredict.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property script_show_inputs
//--- input parameters
input datetime StartDate=D'2015.01.05 00:00:00';
input datetime StopDate=D'2020.08.28 00:00:00';
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
//---
   MqlDateTime DayOfWeek;
   int FirstBar = iBarShift(_Symbol,PERIOD_D1,StartDate);
   int LastBar = iBarShift(_Symbol,PERIOD_D1,StopDate);
   int FileHandle = FileOpen("MondayPredict.csv",FILE_READ|FILE_WRITE|FILE_CSV,0);
   string FileLine="\"Week\",\"Monday\",\"Tuesday\",\"Wednesday\",\"Thursday\",\"Friday\"";
   FileWrite(FileHandle,FileLine);
   FileLine = "\""+IntegerToString(iBarShift(_Symbol,PERIOD_W1,iTime(_Symbol,PERIOD_D1,FirstBar)))+"\",\"";
   for(int i=FirstBar;i>=LastBar;i--){
      FileLine = FileLine+DoubleToString(iClose(_Symbol,PERIOD_D1,i)-iOpen(_Symbol,PERIOD_D1,i))+"\"";
      TimeToStruct(iTime(_Symbol,PERIOD_D1,i),DayOfWeek);
      if(DayOfWeek.day_of_week==5){
         FileWrite(FileHandle,FileLine);
         FileLine = "\""+IntegerToString(iBarShift(_Symbol,PERIOD_W1,iTime(_Symbol,PERIOD_D1,i-1)))+"\",\"";
      } else {
         FileLine = FileLine + ",\"";
      }
   }
   
   
}
//+------------------------------------------------------------------+
