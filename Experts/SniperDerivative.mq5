//+------------------------------------------------------------------+
//|                                             SniperDerivative.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
#include <Trade\Trade.mqh>
//--- input parameters
input int      VolumeCheck=100;     // Volume level upon which we consider a candle to be viable.
input int      StartHour=9;         // Hour of the day on which the EA starts entering trades.
input int      StopHour=22;         // Hour of the day on which the EA stops entering trades.
input int      TPoints=10;          // Points from the closing of the last candle to move for Take Profit.
input bool     MA200=true;          // Do we check if Volume candle is above or below the MA200 line.
input int      PrevMA200=0;         // How many candles before the Volume candle have to be on the same side of the MA200 line.
input bool     MA10Next=true;       // Check if the last candle is above or below the MA10 line.
input bool     MA200Next=false;     // Check if the last candle is above or below the MA200 line.
input int      BarsCheck=15;        // How many candles back do we check for Volume candles.
input double   iTradeVolume=0.01;   // Base volume for trading.
//--- global parameters
CTrade trade;
int MA200Handle,MA10Handle,VolumeHandle,OldArray=0;
datetime PositionEntries[]; // Array of opening time of the Volume candle used in opening a position.
bool PositionBuy[]; // Array of whether a positions is long (true) or short (false).
ulong PositionTickets[]; // Array of tickets for openend positions.
datetime OldEntries[]; // Array holding the opening time of the already used Volume cnadles.
int OldNumber=BarsCheck+5; // Size of the OldEntries array.
int LastCheck = D'2015.01.05 00:00:00'; // datetime of the last candle checked.
string EAMagic="1000005";
string EAComment = "SniperDerivative";
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
//---
   if(StartHour>StopHour) return(INIT_PARAMETERS_INCORRECT); 
   if(StartHour<0 || StopHour<0 || TPoints<0 || PrevMA200<0) return(INIT_PARAMETERS_INCORRECT);
   EAMagic = EAMagic+_Symbol;
   EAComment = EAComment+_Symbol;
   trade.SetExpertMagicNumber(StringToInteger(EAMagic));
   MA10Handle = iMA(_Symbol,_Period,10,0,MODE_EMA,PRICE_CLOSE);
   MA200Handle = iMA(_Symbol,_Period,200,0,MODE_EMA,PRICE_CLOSE);
   VolumeHandle = iVolumes(_Symbol,_Period,VOLUME_TICK);
   ArrayResize(OldEntries,OldNumber);
   //ArrayResize(PositionTickets,10,10);
   //ArrayResize(PositionBuy,10,10);
   //ArrayResize(PositionEntries,10,10);
   
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
   datetime CurBarTime = iTime(_Symbol,_Period,0);
   // Check if any of the opened positions has closed.
   if(ArraySize(PositionTickets) > 0) {
      CheckPosClose(PositionTickets,PositionEntries);
   }
   // We are entering trades and checking MA10 crossovers on the opening of every bar.
   if(CurBarTime != LastCheck){
      long Volume[];
      ArraySetAsSeries(Volume,true);
      CopyTickVolume(_Symbol,_Period,1,BarsCheck,Volume);
      bool VolumeEnter=false;
      int PrevBars[],PrevBNum=0;
      // For the BarsCheck previous candles we check if volume>VolumeCheck and add it's index to an array in that case.
      for(int i=0;i<BarsCheck;i++){
         if(Volume[i]>VolumeCheck){
            PrevBNum = PrevBNum+1;
            ArrayResize(PrevBars,PrevBNum,5);
            PrevBars[PrevBNum-1] = i+1;
            VolumeEnter = true;
         }
      }
      MqlDateTime CurrentHour;
      TimeCurrent(CurrentHour);
      if(CurrentHour.hour < StartHour || (CurrentHour.hour > StopHour && StopHour != 0)) VolumeEnter = false;
      // We check if any of our open positions has crossed over the MA10 line and closing it in that case.
      if(ArraySize(PositionTickets)>0){
         bool MA10Test;
         for(int i=0;i<ArraySize(PositionTickets);i++){
            MA10Test = CheckMA10Close(MA10Handle);
            if((PositionBuy[i]&&!MA10Test) || (!PositionBuy[i]&&MA10Test)){
               trade.PositionClose(PositionTickets[i]);
            }
         }
         CheckPosClose(PositionTickets,PositionEntries);
      }
      
      // If any of the BarsCheck previous candles has volume>VolumeCheck we check futher conditions to initiate a trade.
      if(VolumeEnter){
         double Close[],VolLow[],VolHigh[];
         CopyClose(_Symbol,_Period,1,1,Close);
         for(int i=0;i<ArraySize(PrevBars);i++){
            VolumeEnter = true;
            CopyLow(_Symbol,_Period,PrevBars[i],1,VolLow);
            CopyHigh(_Symbol,_Period,PrevBars[i],1,VolHigh);
            // We check if any of the Volume candles has already been used before.
            for(int j=0;j<ArraySize(OldEntries);j++){
               if(OldEntries[j] == iTime(_Symbol,_Period,PrevBars[i])) VolumeEnter = false;
            }
            int UpOrDown=0;
            if(Close[0]>VolHigh[0]) UpOrDown=1; // Break above Volume candle.
            if(Close[0]<VolLow[0]) UpOrDown=-1; // Break below Volume candle.
            // Check if there is a break of an unused Volume candle.
            if(VolumeEnter && (UpOrDown==1 || UpOrDown==-1)){
               bool MA200Test,MA10Test;
               int MA200VolTest,MA200NextTest;
               // Check if all PrevMA200 candles are above the MA200 line.
               MA200Test = CheckMA200(MA200Handle,PrevBars[i],true,PrevMA200);
               // Check if the Volume candle is above (true) or below (false) the MA200 line.
               MA200VolTest = CheckUpDownMA200(MA200Handle,PrevBars[i]);
               // Check if the last candle closes above (true) or below (false) the MA10 line.
               MA10Test = CheckMA10Close(MA10Handle) || !MA10Next;
               // Check if the last candle is above (true) or below (false) the MA200 line.
               MA200NextTest = CheckUpDownMA200(MA200Handle,1);
               bool BuyTest = MA200Test && (MA10Test||!MA10Next)&&(MA200VolTest==1||!MA200)&&(MA200NextTest==1||!MA200Next);
               // Are the conditions met to enter a long trade?
               if(BuyTest){
                  double TP = Close[0]+TPoints*_Point; // Take profit
                  double SL = 0.0;//VolLow[0]; // Backup StopLoss
                  double TradeVolume = iTradeVolume; // Volume to trade.
                  trade.Buy(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),SL,TP,EAComment);
                  // We update the arrays with the data for the currently opened positions.
                  int Size = ArraySize(PositionTickets);
                  ArrayResize(PositionTickets,Size+1,10);
                  ArrayResize(PositionBuy,Size+1,10);
                  ArrayResize(PositionEntries,Size+1,10);
                  PositionBuy[Size] = true;
                  PositionEntries[Size] = iTime(_Symbol,_Period,PrevBars[i]);
                  PositionTickets[Size] = trade.ResultOrder();
                  // We update the array for the already used Volume cnadles.
                  OldEntries[OldArray%(OldNumber)] = iTime(_Symbol,_Period,PrevBars[i]);
                  OldArray = OldArray+1;
               }
               // Check if all PrevMA200 candles are below the MA200 line.
               MA200Test = CheckMA200(MA200Handle,PrevBars[i],false,PrevMA200);
               bool SellTest = MA200Test && (!MA10Test||!MA10Next)&&(MA200VolTest==-1||!MA200)&&(MA200NextTest==-1||!MA200Next);
               if(SellTest){
                  double TP = Close[0]-TPoints*_Point; // Take profit
                  double SL = 0.0;//VolHigh[0]; // Backup StopLoss
                  double TradeVolume = iTradeVolume; // Volume to trade.
                  trade.Sell(TradeVolume,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),SL,TP,EAComment);
                  // We update the arrays with the data for the currently opened positions.
                  int Size = ArraySize(PositionTickets);
                  ArrayResize(PositionTickets,Size+1,10);
                  ArrayResize(PositionBuy,Size+1,10);
                  ArrayResize(PositionEntries,Size+1,10);
                  PositionBuy[Size] = false;
                  PositionEntries[Size] = iTime(_Symbol,_Period,PrevBars[i]);
                  PositionTickets[Size] = trade.ResultOrder();
                  // We update the array for the already used Volume cnadles.
                  OldEntries[OldArray%(OldNumber)] = iTime(_Symbol,_Period,PrevBars[i]);
                  OldArray = OldArray+1;
               }
            }
         }
         
         
      }
      
   }
   
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade(){
   CheckPosClose(PositionTickets,PositionEntries);
   //int Remaining=0,RemTickets[],AllPositions = PositionsTotal();
   //datetime RemEntries[];
   //bool Remains;
   //for(int i=0;i<ArraySize(PositionTickets);i++){
   //   Remains = false;
   //   for(int j=0;j<AllPositions;j++){
   //      if(PositionGetTicket[j] == PositionTickets[i]) Remains=true;
   //   }
   //   if(Remains){
   //      Remaining = Remaining+1;
   //      ArrayResize(RemTickets,Remaining,0);
   //      ArrayResize(RemEntries,Remaining,0);
   //      RemTickets[Remaining-1] = PositionTickets[i];
   //      RemEntries[Remaining-1] = PositionEntries[i];
   //   }
   //}
   //if(Remaining==0){
   //   ArrayFree(PositionEntries);
   //   ArrayFree(PositionTickets);
   //} else {
   //   ArrayResize(PositionEntries,Remaining,0);
   //   ArrayResize(PositionTickets,Remaining,0);
   //   ArrayCopy(PositionEntries,RemEntries);
   //   ArrayCopy(PositionTickets,RemTickets);
   //}
}
//+------------------------------------------------------------------+
//| Check if any Position has closed.                                |
//+------------------------------------------------------------------+
void CheckPosClose(ulong &TicketArray[],datetime &EntryVector[]){
   int Remaining=0,AllPositions = PositionsTotal();
   ulong RemTickets[];
   datetime RemEntries[];
   bool Remains;
   for(int i=0;i<ArraySize(TicketArray);i++){
      Remains = false;
      for(int j=0;j<AllPositions;j++){
         if(PositionGetTicket(j) == TicketArray[i]) Remains=true;
      }
      if(Remains){
         Remaining = Remaining+1;
         ArrayResize(RemTickets,Remaining,0);
         ArrayResize(RemEntries,Remaining,0);
         RemTickets[Remaining-1] = TicketArray[i];
         RemEntries[Remaining-1] = EntryVector[i];
      }
   }
   if(Remaining==0){
      ArrayFree(EntryVector);
      ArrayFree(TicketArray);
   } else {
      ArrayResize(EntryVector,Remaining,0);
      ArrayResize(TicketArray,Remaining,0);
      ArrayCopy(EntryVector,RemEntries);
      ArrayCopy(TicketArray,RemTickets);
   }
   
   
}
//+------------------------------------------------------------------+
//| Check Previous MA200vsClose value function                       |
//+------------------------------------------------------------------+
bool CheckMA200(int MAHandle,int VolumeBar,bool Up=true,int Bars2Check = 10){
   if(Bars2Check==0) return(true);
   double MA200Val[],Close[];
   CopyBuffer(MAHandle,0,VolumeBar+1,Bars2Check,MA200Val);
   CopyClose(_Symbol,_Period,VolumeBar+1,Bars2Check,Close);
   
   for(int i=0;i<Bars2Check;i++){
      if(Up && MA200Val[i] > Close[i]) return(false);
      if(!Up && MA200Val[i] < Close[i]) return(false);
   }
   return(true);
   
}

//+------------------------------------------------------------------+
//| Check wether Vol candle is above or below MA200 function         |
//+------------------------------------------------------------------+
int CheckUpDownMA200(int MAHandle, int VolumeBar){
   double MA200Val[],Low[],High[];
   CopyBuffer(MAHandle,0,VolumeBar,1,MA200Val);
   CopyLow(_Symbol,_Period,VolumeBar,1,Low);
   CopyHigh(_Symbol,_Period,VolumeBar,1,High);
   
   if(MA200Val[0] < Low[0]) return(1);
   if(MA200Val[0] > High[0]) return(-1);
   return(0);
   
}
//+------------------------------------------------------------------+
//| Check MA10 at last close function                                |
//+------------------------------------------------------------------+
bool CheckMA10Close(int MAHandle){
   double MABuffer[],Close[];
   CopyBuffer(MAHandle,0,1,1,MABuffer);
   CopyClose(_Symbol,_Period,1,1,Close);
   if(Close[0]>MABuffer[0]) return(true);
   return(false);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+