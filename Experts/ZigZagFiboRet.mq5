//+------------------------------------------------------------------+
//|                                                ZigZagFiboRet.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
//--- input parameters
enum Retrace{
   Only38 = 0,
   Only50 = 1,
   Both = 2
};

input Retrace  RetraceType;
input double   Risk=0.02; //-- Maximum percentage of account to risk in any one operation.
input int      LookBack=25;
input bool     MASide=true;
input bool     MACross=true;
input int      MALength=500;
input int      MinImpulseSize=1000;
input int      ZZLookBack=2;
//--- Global Variables
int MA250Handle,MAVHandle,ZZHandle;
double 38Level[],50Level[],61Level[],BoxLevel[];
bool 38Entry[],50Entry[];
ulong 38Tickets[],50Tickets[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   
//---
   ZZHandle = iCustom(_Symbol,_Period,"Examples/ZigZag",30,5,3);
   MA250Handle = iMA(_Symbol,_Period,250,0,MODE_EMA,PRICE_CLOSE);
   MAVHandle = iMA(_Symbol,_Period,MALength,0,MODE_EMA,PRICE_CLOSE);
   ArrayResize(38Level,ZZLookBack,0);
   ArrayResize(50Level,ZZLookBack,0);
   ArrayResize(61Level,ZZLookBack,0);
   ArrayResize(BoxLevel,ZZLookBack,0);
   ArrayResize(38Entry,ZZLookBack,0);
   ArrayResize(38Entry,ZZLookBack,0);
   ArrayResize(38Tickets,ZZLookBack,0);
   ArrayResize(38Tickets,ZZLookBack,0);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade(){
//---

}
//+------------------------------------------------------------------+
