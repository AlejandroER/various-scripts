//+------------------------------------------------------------------+
//|                                      ExtractSniperDerivative.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property script_show_inputs
//+------------------------------------------------------------------+
//| Input Parameters.                                                |
//+------------------------------------------------------------------+
input datetime StartDate=D'2018.01.01 00:00:00';
//+------------------------------------------------------------------+
//| Global Parameters.                                               |
//+------------------------------------------------------------------+
int ForwardBars = 15;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart(){
   double Close[],High[],Low[],PrevClose[],MA200[],MA10[];
   long Volume[];
   int Filehandle = FileOpen("SniperDeriv.csv",FILE_READ|FILE_WRITE|FILE_CSV,0,CP_UTF8);
   string ToWrite = "\"Hour\",\"Minute\",\"Volume\",\"High0\",\"Low0\",\"MA200_0\"";
   int MA200Handle = iMA(_Symbol,_Period,200,0,MODE_EMA,PRICE_CLOSE);
   int MA10Handle = iMA(_Symbol,_Period,10,0,MODE_EMA,PRICE_CLOSE);
   
   for(int i=1;i<=10;i++){
      ToWrite = ToWrite + ",\"PrevClose" + IntegerToString(i) + "\"";
      ToWrite = ToWrite + ",\"MA200_" + IntegerToString(i) + "\"";
   }
   for(int i=1;i<=ForwardBars;i++){
      ToWrite = ToWrite + ",\"Close" + IntegerToString(i) + "\"";
      ToWrite = ToWrite + ",\"High" + IntegerToString(i) + "\"";
      ToWrite = ToWrite + ",\"Low" + IntegerToString(i) + "\"";
      ToWrite = ToWrite + ",\"MA10_" + IntegerToString(i) + "\"";
   }
   FileWrite(Filehandle,ToWrite);
   datetime BarDate;
   MqlDateTime BarDateTime;
   ArraySetAsSeries(Close,true);
   ArraySetAsSeries(High,true);
   ArraySetAsSeries(Low,true);
   ArraySetAsSeries(PrevClose,true);
   ArraySetAsSeries(MA200,true);
   ArraySetAsSeries(MA10,true);
   ArraySetAsSeries(Volume,true);
   
   int CheckBars = Bars(_Symbol,_Period,StartDate,TimeCurrent());
   //Alert(CheckBars);
   for(int i=ForwardBars;i<CheckBars;i++){
      CopyTickVolume(_Symbol,_Period,i,1,Volume);
      if(Volume[0]>100){
         //Alert(Volume[0]);
         BarDate = iTime(_Symbol,_Period,i);
         TimeToStruct(BarDate,BarDateTime);
         CopyHigh(_Symbol,_Period,i-ForwardBars,ForwardBars+1,High);
         CopyLow(_Symbol,_Period,i-ForwardBars,ForwardBars+1,Low);
         CopyClose(_Symbol,_Period,i-ForwardBars,ForwardBars,Close);
         CopyClose(_Symbol,_Period,i+1,10,PrevClose);
         CopyBuffer(MA200Handle,0,i,11,MA200);
         CopyBuffer(MA10Handle,0,i-ForwardBars,ForwardBars,MA10);
         ToWrite = "\""+IntegerToString(BarDateTime.hour)+"\",\""+IntegerToString(BarDateTime.min)+"\",\""+IntegerToString(Volume[0])+"\"";
         ToWrite = ToWrite +",\""+DoubleToString(High[ForwardBars])+"\",\""+DoubleToString(Low[ForwardBars])+"\",\""+DoubleToString(MA200[0])+"\"";
         //Alert(ToWrite);
         for(int j=0;j<10;j++){
            ToWrite = ToWrite + ",\"" + DoubleToString(PrevClose[j]) + "\"";
            ToWrite = ToWrite + ",\"" + DoubleToString(MA200[j+1]) + "\"";
         }
         for(int j=ForwardBars-1;j>=0;j--){
            ToWrite = ToWrite + ",\"" + DoubleToString(Close[j]) + "\"";
            ToWrite = ToWrite + ",\"" + DoubleToString(High[j]) + "\"";
            ToWrite = ToWrite + ",\"" + DoubleToString(Low[j]) + "\"";
            ToWrite = ToWrite + ",\"" + DoubleToString(MA10[j]) + "\"";
         }
         FileWrite(Filehandle,ToWrite);
      }
   }
   
   
}
//+------------------------------------------------------------------+
