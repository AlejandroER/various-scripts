   //+------------------------------------------------------------------+
//|                                                      Ranging.mq5 |
//|                                                 Alejandro Escuer |
//|                                             |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property version   "2.00"
#include <Trade\Trade.mqh>


input int barNum = 20;                 // The number of bars the EA will look back to check if the market is ranging.
input int FractSep = 3;                // How far apart must two fractals be to be taken into consideration for IsRanging.
input int FractNum = 2;                // Number of Fractals needed to declare Price to be Ranging.
input int FractSize = 5;               // Number of bars used in calculating a fractal.
input double RangeBorderBuffer = 0.2;  // Percentile of the Range whithin which must be the two highest and two lowest fractals.
input double StartVolume = 0.1;        // Starting Volume of operation for Money Management.
input double StartCapital = 300;       // Starting Capital available to the EA.
input double EAProfit = 0;             // The amount of money the EA has earned over it's life. Used for MoneyManagement.
input int ExtraSLPoints = 0;           // How many points are added to the Border + Spread + 1 base StopLoss.
input double PercRangePrice = 0.2;     // Percentile of the Range where the entry into the market is to be set.
input double PercRangeTP = 0.6;        // Percentile of the Range where TakeProfit is to be set.
bool IsInRange = false;                // Have we already determined that the market is ranging.
double RangeHeight,Lowest,Highest;     // Variables where we keep the results of the IsRanging function. Size of range, Lowest fractal and Highest fractal respectively.
ulong OrderTicket = 0;                 // Ticket of the last opened order.
CTrade trade;                          // CTrade object for ease of use of the SendOrder function.

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
   
   if(barNum < FractNum*FractSep || PercRangePrice > PercRangeTP){
      return(INIT_PARAMETERS_INCORRECT);
   }
   
   if(FractNum < 2 || FractSize%2 == 0 || FractSize < 3){
      return(INIT_PARAMETERS_INCORRECT);
   }
   
   if(barNum<0||FractSep<0||RangeBorderBuffer<=0||StartVolume<=0||StartCapital<=0||PercRangePrice<=0||PercRangeTP<=0){
      return(INIT_PARAMETERS_INCORRECT);
   }
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
      double HighPrices[],LowPrices[];
      ArraySetAsSeries(HighPrices,true);
      ArraySetAsSeries(LowPrices,true);
      ArrayResize(HighPrices,barNum);
      ArrayResize(LowPrices,barNum);
      string EAComment;
      
      // Here we build the Comment that will serve as identifier for the Orders and Positions opended by this EA.
      EAComment = "RangingEA: "+_Symbol+","+string(_Period);
      
      // We check if it is already declared to be ranging.
      if(IsInRange){
      
         ulong CloseOrderTicket;
         // We check if the Price has left the range and close any order opened by the EA if it has.
         if(SymbolInfoDouble(_Symbol,SYMBOL_BID) > Highest || SymbolInfoDouble(_Symbol,SYMBOL_ASK) < Lowest){
            IsInRange = false;
            bool OrderExists = true;
            while(OrderExists){
               OrderExists = false;
               // Here we iterate over all open orders checking if any has been opened by this EA.
               for(int i=0;i<OrdersTotal();i++){
                  CloseOrderTicket = OrderGetTicket(i);
                  if(!OrderSelect(CloseOrderTicket)){
                     Sleep(1000);
                     if(!OrderSelect(CloseOrderTicket)){
                        CloseOrderTicket = 0;
                     }
                  }
                  // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
                  if(OrderGetString(ORDER_COMMENT) == EAComment){
                     OrderExists = true;
                     trade.OrderDelete(CloseOrderTicket);
                     break;
                  }
               }
            }
         } else {
            // Price has not yet left the Range.
            bool OrderExists = false;
            // First we will check if there is an order or a position already open.
            for(int i=0;i<OrdersTotal();i++){
               CloseOrderTicket = OrderGetTicket(i);
               if(!OrderSelect(CloseOrderTicket)){
                     Sleep(1000);
                     if(!OrderSelect(CloseOrderTicket)){
                        CloseOrderTicket = 0;
                     }
                  }
               // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
               if(OrderGetString(ORDER_COMMENT) == EAComment){
                  OrderExists = true;
                  trade.OrderDelete(CloseOrderTicket);
                  break;
               }
            }
            // If we have not found an open order we check the existing positions.
            if(!OrderExists){
               for(int i=0;i<PositionsTotal();i++){
                  CloseOrderTicket = PositionGetTicket(i);
                  if(!OrderSelect(CloseOrderTicket)){
                     Sleep(1000);
                     if(!OrderSelect(CloseOrderTicket)){
                        CloseOrderTicket = 0;
                     }
                  }
                  // We check the comment of each Position and compare it to the Identifier we set for this EA in each Symbol and Period.
                  if(PositionGetString(POSITION_COMMENT) == EAComment){
                     OrderExists = true;
                     break;
                  }
               }
            }
            
            if(!OrderExists){
               
               double OrderVolume = StartVolume*floor((StartCapital+EAProfit)/StartCapital);
               if(SymbolInfoDouble(_Symbol,SYMBOL_BID) > (Highest - RangeHeight*PercRangePrice)){
                  double OrderPrice = MathRound((Highest - RangeHeight*PercRangePrice)/_Point)*_Point;
                  double OrderTP = MathRound((Highest - RangeHeight*PercRangeTP)/_Point)*_Point;
                  double OrderSL = MathRound((Highest + SymbolInfoDouble(_Symbol,SYMBOL_ASK) - SymbolInfoDouble(_Symbol,SYMBOL_BID) + 1*_Point + ExtraSLPoints*_Point)/_Point)*_Point;
                  if(!trade.SellStop(OrderVolume,OrderPrice,_Symbol,OrderSL,OrderTP,ORDER_TIME_GTC,0,EAComment)){
                     Alert("SellStop Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                     
                  }
                  
               } else if(SymbolInfoDouble(_Symbol,SYMBOL_ASK) < (Lowest + RangeHeight*PercRangePrice)){
                  double OrderPrice = MathRound((Lowest + RangeHeight*PercRangePrice)/_Point)*_Point;
                  double OrderTP = MathRound((Lowest + RangeHeight*PercRangeTP)/_Point)*_Point;
                  double OrderSL = MathRound((Lowest - SymbolInfoDouble(_Symbol,SYMBOL_ASK) + SymbolInfoDouble(_Symbol,SYMBOL_BID) - 1*_Point - ExtraSLPoints*_Point)/_Point)*_Point;
                  if(!trade.BuyStop(OrderVolume,OrderPrice,_Symbol,OrderSL,OrderTP,ORDER_TIME_GTC,0,EAComment)){
                     Alert("SellStop Failed with RetCode: ", trade.ResultRetcodeDescription()," (",trade.ResultRetcode(),")");
                     
                  }
               }
            }
            
         }
      } else {
      // Here we extract the High and Low prices of the past barNum candles, the current one not included.
         if(CopyHigh(_Symbol,_Period,1,barNum,HighPrices)==-1){
            Sleep(2000);
            CopyHigh(_Symbol,_Period,1,barNum,HighPrices);
         }
         if(CopyLow(_Symbol,_Period,1,barNum,LowPrices)==-1){
            Sleep(2000);
            CopyLow(_Symbol,_Period,1,barNum,LowPrices);
         }
         
         IsInRange = IsRanging(HighPrices,LowPrices); // ,RangeHeight,Highest,Lowest,barNum,FractSep,RangeBorderBuffer);
         if(IsInRange){
            Print("Price is in Range");
         }
      }
      
      
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bearish Fractal for the whole sequence.
bool IsUpFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] > Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// We check if the center bar of the sequence is a Bullish Fractal for the whole sequence.
bool IsDownFractal(double &Prices[],int barsNum)
{
   if(barsNum%2==0){
      Alert("Para calcular fractales se necesita un número impar de velas.");
      ExpertRemove();
   }
   
   // Check the bars.
   for(int i=0;i<barsNum;i++){
      if(Prices[i] < Prices[(barsNum-1)/2]){
         return(false);
      }
   }
   return(true);
}
//+------------------------------------------------------------------+
// In this function we check if the Market is ranging or not.
bool IsRanging(double &HighPrice[],double &LowPrice[])   // ,double &RangeHeight,double &Highest, double &Lowest, int barNum,int FractSep, double RangeBorderBuffer)
{
   double UpFractals[],DownFractals[],TestBars[];
   //int FractSize = 5;                              // This is how many bars it takes to form a fractal.
   // ArrayResize(TestBars,FractSize); 
   int UpFBarIndex[],DownFBarIndex[],HalfFract,Down = 0,Up = 0,BestHigh1 = 0,BestLow1 = 0,BestHigh2 = 0,BestLow2 = 0,BestHigh[],BestLow[],LastBest=0;
   
   HalfFract = (FractSize-1)/2;
   
   ArrayResize(UpFractals,barNum);
   ArrayResize(DownFractals,barNum);
   ArrayResize(UpFBarIndex,barNum);
   ArrayResize(DownFBarIndex,barNum);
   ArrayResize(BestHigh,FractNum);
   ArrayResize(BestLow,FractNum);
   ArrayFill(BestHigh,0,FractNum,0);
   ArrayFill(BestLow,0,FractNum,0);
   // We populate two vectors for Up and Down fractals, one of the values at fractal and another of the candle number at which the fractal appears.
   for(int i=HalfFract;i<barNum-HalfFract;i++){
      ArrayCopy(TestBars,HighPrice,0,i-HalfFract,FractSize);
      // TestBars[0] = HighPrice[i-2]; TestBars[1] = HighPrice[i-1]; TestBars[2] = HighPrice[i]; TestBars[3] = HighPrice[i+1]; TestBars[4] = HighPrice[i+2];
      if(IsUpFractal(TestBars,FractSize)){
         UpFractals[Up] = HighPrice[i];
         UpFBarIndex[Up] = i;
         Up++;
         LastBest = ArrayMaximum(BestHigh);
         // Here we find the FractNum highest of all the fractals that are more than FractSep appart.
         
         if(HighPrice[0] < HighPrice[i]){
            if(i - BestHigh[LastBest] > FractSep){
               for(int j=FractNum-1;j>0;j--){
                  BestHigh[j] = BestHigh[j-1];
               }
               BestHigh[0] = i;
            } else {
               for(int j=LastBest;j>0;j--){
                  BestHigh[j] = BestHigh[j-1];
               }
               BestHigh[0] = i;
            }
         } else if(HighPrice[BestHigh[FractNum-1]] < HighPrice[i]){
            for(int j=FractNum-2;j>=0;j--){
               if(HighPrice[BestHigh[j]] >= HighPrice[i]){
                  if(i - BestHigh[LastBest] > FractSep){
                     for(int k=FractNum-1;k>j;k--){
                        BestHigh[k] = BestHigh[k-1];
                     }
                     BestHigh[j+1] = i;
                     break;
                  } else {
                     if(HighPrice[i] > HighPrice[LastBest]){
                        for(int k=LastBest;k>j;k--){
                           BestHigh[k] = BestHigh[k-1];
                        }
                        BestHigh[j+1] = i;
                        break;
                     } else {
                        break;
                     }
                  }
               }
            }
         }
         
         //if(HighPrice[i] > HighPrice[BestHigh1]){
         //   if(i-BestHigh1 > FractSep){
         //      BestHigh2 = BestHigh1;
         //   }
         //   BestHigh1 = i;
         //}
      }
      
      ArrayCopy(TestBars,LowPrice,0,i-HalfFract,FractSize);
      // TestBars[0] = LowPrice[i-2]; TestBars[1] = LowPrice[i-1]; TestBars[2] = LowPrice[i]; TestBars[3] = LowPrice[i+1]; TestBars[4] = LowPrice[i+2];
      if(IsDownFractal(TestBars,FractSize)){
      // Alert("Bar:",i,"----",LowPrice[i]);
         DownFractals[Down] = LowPrice[i];
         DownFBarIndex[Down] = i;
         Down++;
         LastBest = ArrayMaximum(BestLow);      // Index of the newest addition to BestLow.
         
         // Here we find the two lowest of all the fractals that are more than FractSep appart.
         if(LowPrice[0] > LowPrice[i]){      // Is new Low Fractal lower than the Lowest fractal we have?
            if(i - BestLow[LastBest] > FractSep){
               for(int j=FractNum-1;j>0;j--){   // Move all indexes and change the Lowest fractal.
                  BestLow[j] = BestLow[j-1];
               }
               BestLow[0] = i;
            } else {
               for(int j=LastBest;j>0;j--){     // Move the indexes of all fractals lower than the LastBest.
                  BestLow[j] = BestLow[j-1];
               }
               BestLow[0] = i;
            }
         } else if(LowPrice[BestLow[FractNum-1]] > LowPrice[i]){     // Is new Low Fractal lower than highest BestLow?
            for(int j=FractNum-2;j>=0;j--){                    // Find which fractal is 
               if(LowPrice[BestLow[j]] <= LowPrice[i]){        //lower than the new one.
                  if(i - BestLow[LastBest] > FractSep){
                     for(int k=FractNum-1;k>j;k--){      // Move all indexes, until right before the fractal just lower than the new one.
                        BestLow[k] = BestLow[k-1];
                     }
                     BestLow[j+1] = i;
                     break;
                  } else {    // New is closer than FractSep away from LastBest.
                     if(LowPrice[i] < LowPrice[LastBest]){
                        for(int k=LastBest;k>j;k--){     // We move all indexes from LastBest, up to right before the fractal just lower than the new one.
                           BestLow[k] = BestLow[k-1];
                        }
                        BestLow[j+1] = i;
                        break;
                     } else { // We keep the LastBest instead of the new fractal because it is lower.
                        break;
                     }
                  }
               }
            }
         }
         
         
         //if(LowPrice[i] < LowPrice[BestLow1]){
         //   if(i-BestLow1 > FractSep){
         //      BestLow2 = BestLow1;
         //   }
         //   BestLow1 = i;
         //}
      }
   }
   //Alert("UP: ",ArraySize(UpFractals)," --- ","DOWN: ",ArraySize(DownFractals));
   //Alert(BestHigh1);
   //Alert(BestLow1);
   //Alert(BestHigh2);
   //Alert(BestLow2);
   //Alert(LowPrice[0]);
   // bool BestZero = false;
   
   //for(int i=0;i<FractNum;i++){
   //   if(BestLow[i] == 0 || BestHigh[i] == 0){
   //      BestZero = true;
   //      break;
   //   }
   //}
   
   //if(BestZero){
   //   return(false);
   //}
   
   if(BestHigh[ArrayMinimum(BestHigh)] == 0 || BestLow[ArrayMinimum(BestLow)] == 0){      // BestHigh and BestLow only contain non-negative integers.
      return(false);                                                                      // If any entry is 0 there are not enough fractals to declare Ranging.
   }
   
   RangeHeight = HighPrice[BestHigh[0]] - LowPrice[BestLow[0]];
   // Alert(RangeHeight*RangeBorderBuffer);
   // Alert(HighPrice[BestHigh1]-HighPrice[BestHigh2]);
   // Alert(LowPrice[BestLow2]-LowPrice[BestLow1]);
   if(HighPrice[BestHigh[0]]-HighPrice[BestHigh[FractNum-1]]<RangeHeight*RangeBorderBuffer && LowPrice[BestLow[0]]-LowPrice[BestLow[FractNum-1]]<RangeHeight*RangeBorderBuffer){
      Highest = HighPrice[BestHigh[0]];
      Lowest = LowPrice[BestLow[0]];
      // Alert(Highest);
      return(true);
   } else {
      return(false);
   }
}
//+------------------------------------------------------------------+

