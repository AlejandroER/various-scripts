//+------------------------------------------------------------------+
//|                                                MondayPredict.mq5 |
//|                                                 Alejandro Escuer |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
#include <Math\Stat\Stat.mqh>
//--- ENUM block
enum AddToTrade{
   None=0,
   MA_23=1,
   Fibonacci=2,
   ManualLevels=3
};
//--- input parameters
input double   Volume = 0.01;
input int EntryCheck = 100;
input AddToTrade AdditionMethod = MA_23;
input bool AdaptVolume = false;
//--- Global Parameters
CTrade trade;
string EAComment;
string EAMagic="001234500";
string FileName="MondayPredictTickets.txt";
int NumTickets=0,FileHandle=0,MA23Handle;
double ATR[],PricesTickets[];
datetime TimeTickets[];
bool TicketOpen=false,MACross=false,PositionBuy=false,PositionSell=false;
ulong OpenTickets[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit(){
//---
   EAMagic = EAMagic+_Symbol;
   EAComment = "MondayPredictExpert"+_Symbol;
   
   int ATRHande = iATR(_Symbol,_Period,48);
   CopyBuffer(ATRHande,0,0,1,ATR);
   if(AdditionMethod==MA_23){
      MA23Handle = iMA(_Symbol,_Period,23,0,MODE_EMA,PRICE_CLOSE);
   }
   
   trade.SetExpertMagicNumber(StringToInteger(EAMagic));
   ArrayResize(OpenTickets,1,4);
   OpenTickets[0] = 0;
   int AllPositions;
   AllPositions = PositionsTotal();
   ulong TestOrderTicket;
   for(int i=0;i<AllPositions;i++){
      TestOrderTicket = PositionGetTicket(i);
      if(!PositionSelectByTicket(TestOrderTicket)){
         Sleep(1000);
         if(!PositionSelectByTicket(TestOrderTicket)){
            TestOrderTicket = 0;
            Sleep(1000);
            return(INIT_FAILED);
         }
      }
      // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
      //and if there is an open position from a previous session we grab that ticket.
      if(PositionGetString(POSITION_COMMENT) == EAComment){
         FileHandle = FileOpen(FileName,FILE_READ|FILE_WRITE|FILE_TXT,0);
         string TicketsString[],Contents=FileReadString(FileHandle);
         NumTickets = StringSplit(Contents,StringGetCharacter(",",0),TicketsString);
         ArrayResize(OpenTickets,NumTickets);
         for(int j=0;i<NumTickets;i++){
            OpenTickets[j] = StringToInteger(TicketsString[j]);
         }
         FileClose(FileHandle);
         MqlDateTime CurrentTime;
         TimeCurrent(CurrentTime);
         PositionSelectByTicket(OpenTickets[0]);
         if((StructToTime(CurrentTime) - PositionGetInteger(POSITION_TIME))>432000){
            for(int j=0;i<NumTickets;j++){
               trade.PositionClose(OpenTickets[j]);
            }
            NumTickets = 0;
            ArrayFree(OpenTickets);
            TicketOpen = false;
         } else {
            ArrayResize(PricesTickets,NumTickets);
            ArrayResize(TimeTickets,NumTickets);
            for(int j=0;i<NumTickets;j++){
               PositionSelectByTicket(OpenTickets[j]);
               PricesTickets[j] = PositionGetDouble(POSITION_PRICE_OPEN);
               //TimeTickets[j] = PositionGetInteger(POSITION_TIME);
               PositionGetInteger(POSITION_TIME,TimeTickets[j]);
            }
            if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY) PositionBuy = true;
            if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL) PositionSell = true;
            TicketOpen = true;
         }
         break;
      }
      
   }
   
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason){
//---
   
   
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){
//---
   datetime PrevBarDay = iTime(_Symbol,PERIOD_D1,1);
   MqlDateTime DayOfWeek,PrevDay;
   TimeToStruct(PrevBarDay,PrevDay);
   TimeCurrent(DayOfWeek);
   Alert(StructToTime(DayOfWeek)," , ",PrevBarDay);
   //Check if it's tuesday and wether a trade is already open.
   //Alert(OpenTickets[0]);
   if(DayOfWeek.day_of_week == 2 && !TicketOpen && PrevDay.day_of_week == 1 ){
      
      
      
      //Check conditions for opening trades.
      //Alert((1/_Point)*(iClose(_Symbol,PERIOD_D1,1)-iOpen(_Symbol,PERIOD_D1,1)));
      if((iClose(_Symbol,PERIOD_D1,1)-iOpen(_Symbol,PERIOD_D1,1))>(EntryCheck*_Point)){
         double TradeVol;
         if(AdaptVolume) {
            TradeVol = MathFloor(1000*_Point/(SymbolInfoDouble(_Symbol,SYMBOL_ASK)-iLow(_Symbol,PERIOD_D1,1))*100)/100;
         } else {
            TradeVol = Volume;
         }
         Alert(TradeVol);
         trade.Buy(TradeVol,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),iLow(_Symbol,PERIOD_D1,1),0.0,EAComment);
         ArrayResize(OpenTickets,1,4);
         ArrayResize(PricesTickets,1,4);
         ArrayResize(TimeTickets,1,4);
         //Sleep(5000);
         OpenTickets[0] = trade.ResultOrder();
         PricesTickets[0] = SymbolInfoDouble(_Symbol,SYMBOL_ASK);
         TimeTickets[0] = TimeCurrent();
         //Alert(OpenTickets[0]);
         NumTickets = 1;
         // Writing Position Tickets to file.
         FileHandle = FileOpen(FileName,FILE_READ|FILE_WRITE|FILE_TXT,0);
         FileWrite(FileHandle,IntegerToString(OpenTickets[0]));
         FileClose(FileHandle);
         PositionBuy = true;
         TicketOpen = true;
      } else if(((iOpen(_Symbol,PERIOD_D1,1)-iClose(_Symbol,PERIOD_D1,1))>(EntryCheck*_Point))){
         double TradeVol;
         if(AdaptVolume) {
            TradeVol = MathFloor(1000*_Point/(iHigh(_Symbol,PERIOD_D1,1)-SymbolInfoDouble(_Symbol,SYMBOL_BID))*100)/100;
         } else {
            TradeVol = Volume;
         }
         Alert(TradeVol);
         trade.Sell(TradeVol,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),iHigh(_Symbol,PERIOD_D1,1),0.0,EAComment);
         ArrayResize(OpenTickets,1,4);
         ArrayResize(PricesTickets,1,4);
         ArrayResize(TimeTickets,1,4);
         //Sleep(5000);
         OpenTickets[0] = trade.ResultOrder();
         PricesTickets[0] = SymbolInfoDouble(_Symbol,SYMBOL_BID);
         TimeTickets[0] = TimeCurrent();
         //Alert(OpenTickets[0]);
         NumTickets = 1;
         // Writing Position Tickets to file.
         FileHandle = FileOpen(FileName,FILE_READ|FILE_WRITE|FILE_TXT,0);
         FileWrite(FileHandle,IntegerToString(OpenTickets[0]));
         FileClose(FileHandle);
         PositionSell = true;
         TicketOpen = true;
      }
   // If we are adding to the trades using MA23 we enter here if a position has been opened.
   } else if(AdditionMethod==MA_23 && TicketOpen && Bars(_Symbol,_Period,TimeTickets[NumTickets-1],TimeCurrent())>5){
      double MABuffer[];
      CopyBuffer(MA23Handle,0,1,1,MABuffer);
      // If the closing price is below the MA and there is a long Position open we prepare to add to the trade.
      if(PositionBuy && !MACross && iClose(_Symbol,_Period,1)<MABuffer[0]){
         MACross = true;
      // If the closing price is above the MA and there is a short Position open we prepare to add to the trade.
      } else if(PositionSell && !MACross && iClose(_Symbol,_Period,1)>MABuffer[0]){
         MACross = true;
      // If there is long Position open and price has crossed back above the MA23 the we add to the trade.
      } else if(MACross && PositionBuy && iClose(_Symbol,_Period,1)>MABuffer[0] && (SymbolInfoDouble(_Symbol,SYMBOL_BID)-PricesTickets[NumTickets-1])>ATR[0]){
         double TradeVol;
         PositionSelectByTicket(OpenTickets[0]);
         TradeVol = PositionGetDouble(POSITION_VOLUME);
         trade.Buy(TradeVol,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_ASK),0.0,0.0,EAComment);
         ArrayResize(OpenTickets,NumTickets+1,3);
         ArrayResize(PricesTickets,NumTickets+1,3);
         ArrayResize(TimeTickets,NumTickets+1,3);
         OpenTickets[NumTickets] = trade.ResultOrder();
         PricesTickets[NumTickets] = SymbolInfoDouble(_Symbol,SYMBOL_ASK);
         TimeTickets[NumTickets] = TimeCurrent();
         FileHandle = FileOpen(FileName,FILE_READ|FILE_WRITE|FILE_TXT,0);
         FileWrite(FileHandle,IntegerToString(OpenTickets[0]));
         FileClose(FileHandle);
         NumTickets = NumTickets+1;
         double StopLoss = MathMean(PricesTickets);
         for(int i=0;i<NumTickets;i++){
            trade.PositionModify(OpenTickets[i],StopLoss,0.0);
         }
         MACross = false;
      // If there is short Position open and price has crossed back below the MA23 the we add to the trade.
      } else if(MACross && PositionSell && iClose(_Symbol,_Period,1)<MABuffer[0] && (PricesTickets[NumTickets-1]-SymbolInfoDouble(_Symbol,SYMBOL_ASK))>ATR[0]){
         double TradeVol;
         PositionSelectByTicket(OpenTickets[0]);
         TradeVol = PositionGetDouble(POSITION_VOLUME);
         trade.Sell(TradeVol,_Symbol,SymbolInfoDouble(_Symbol,SYMBOL_BID),0.0,0.0,EAComment);
         ArrayResize(OpenTickets,NumTickets+1,3);
         ArrayResize(PricesTickets,NumTickets+1,3);
         ArrayResize(TimeTickets,NumTickets+1,3);
         OpenTickets[NumTickets] = trade.ResultOrder();
         PricesTickets[NumTickets] = SymbolInfoDouble(_Symbol,SYMBOL_BID);
         TimeTickets[NumTickets] = TimeCurrent();
         FileHandle = FileOpen(FileName,FILE_READ|FILE_WRITE|FILE_TXT,0);
         FileWrite(FileHandle,IntegerToString(OpenTickets[0]));
         FileClose(FileHandle);
         NumTickets = NumTickets+1;
         double StopLoss = MathMean(PricesTickets);
         for(int i=0;i<NumTickets;i++){
            trade.PositionModify(OpenTickets[i],StopLoss,0.0);
         }
         MACross = false;
      // We check if the open Position has closed by reaching Stop Loss.
      } else {
         bool PositionExists=false;
         int AllPositions;
         AllPositions = PositionsTotal();
         ulong TestOrderTicket;
         for(int i=0;i<AllPositions;i++){
            TestOrderTicket = PositionGetTicket(i);
            if(!PositionSelectByTicket(TestOrderTicket)){
               Sleep(1000);
               PositionSelectByTicket(TestOrderTicket);
            }
            // We check the comment of each order and compare it to the Identifier we set for this EA in each Symbol and Period.
            //and if there is an open position from a previous session we grab that ticket.
            if(PositionGetString(POSITION_COMMENT) == EAComment) PositionExists = true;
         }
         if(!PositionExists){
            PositionBuy = false;
            PositionSell = false;
            TicketOpen =false;
            MACross = false;
            ArrayFree(OpenTickets);
            ArrayFree(PricesTickets);
            ArrayFree(TimeTickets);
         }
      }
   } else if((DayOfWeek.day_of_week == 5 && TicketOpen && DayOfWeek.hour==23 && DayOfWeek.min>30)||(DayOfWeek.day_of_week ==1 && TicketOpen)){
      for(int i=0;i<NumTickets;i++){
         trade.PositionClose(OpenTickets[i]);
      }
      PositionBuy = false;
      PositionSell = false;
      TicketOpen =false;
      MACross = false;
      ArrayFree(OpenTickets);
      ArrayFree(PricesTickets);
      ArrayFree(TimeTickets);
   }
   
}
//+------------------------------------------------------------------+
