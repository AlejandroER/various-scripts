//+------------------------------------------------------------------+
//|                                                  PivotPoints.mq5 |
//|                                                 Alejandro Escuer |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Alejandro Escuer"
#property link      ""
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 7
#property indicator_plots   7
//--- plot Pivot
#property indicator_label1  "Pivot"
#property indicator_type1   DRAW_NONE
#property indicator_color1  clrYellow
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot R1
#property indicator_label2  "R1"
#property indicator_type2   DRAW_NONE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  3
//--- plot S1
#property indicator_label3  "S1"
#property indicator_type3   DRAW_NONE
#property indicator_color3  clrBlue
#property indicator_style3  STYLE_SOLID
#property indicator_width3  3
//--- plot R2
#property indicator_label4  "R2"
#property indicator_type4   DRAW_LINE
#property indicator_color4  clrRed
#property indicator_style4  STYLE_SOLID
#property indicator_width4  2
//--- plot S2
#property indicator_label5  "S2"
#property indicator_type5   DRAW_LINE
#property indicator_color5  clrBlue
#property indicator_style5  STYLE_SOLID
#property indicator_width5  2
//--- plot R3
#property indicator_label6  "R3"
#property indicator_type6   DRAW_NONE
#property indicator_color6  clrRed
#property indicator_style6  STYLE_SOLID
#property indicator_width6  1
//--- plot S3
#property indicator_label7  "S3"
#property indicator_type7   DRAW_NONE
#property indicator_color7  clrBlue
#property indicator_style7  STYLE_SOLID
#property indicator_width7  1
//--- indicator buffers
double         PivotBuffer[];
double         R1Buffer[];
double         S1Buffer[];
double         R2Buffer[];
double         S2Buffer[];
double         R3Buffer[];
double         S3Buffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit(){
//--- indicator buffers mapping
   SetIndexBuffer(0,PivotBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,R1Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,S1Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,R2Buffer,INDICATOR_DATA);
   SetIndexBuffer(4,S2Buffer,INDICATOR_DATA);
   SetIndexBuffer(5,R3Buffer,INDICATOR_DATA);
   SetIndexBuffer(6,S3Buffer,INDICATOR_DATA);
   
   
   ArraySetAsSeries(PivotBuffer,true);
   ArraySetAsSeries(R1Buffer,true);
   ArraySetAsSeries(S1Buffer,true);
   ArraySetAsSeries(R2Buffer,true);
   ArraySetAsSeries(S2Buffer,true);
   ArraySetAsSeries(R3Buffer,true);
   ArraySetAsSeries(S3Buffer,true);
   
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]){
//---
   if(prev_calculated == rates_total) return(rates_total); // If there are no more bars we change nothing.
   int first,PrevDayBar;
   MqlRates Rates[];
   datetime BarTime;
   double Pivot,R1,R2,R3,S1,S2,S3;
   if(prev_calculated == 0){  
      first = rates_total;
   } else {
      first = rates_total - prev_calculated;
   }
   for(int i=0;i<first;i++){
      // We find the D1 bar corresponding to the ith bar and get its Rates data.
      BarTime = iTime(_Symbol,PERIOD_CURRENT,i);
      PrevDayBar = iBarShift(_Symbol,PERIOD_D1,BarTime,false) +1;
      CopyRates(_Symbol,PERIOD_D1,PrevDayBar,1,Rates);
      
      // We calculate the values for the different support and resistance levels ant the bar i.
      Pivot=(Rates[0].high+Rates[0].close+Rates[0].low)/3;
      R1 = (2 * Pivot) - Rates[0].low;
      S1 = (2 * Pivot) - Rates[0].high;
      R2 = Pivot + (R1 - S1);
      R3 = Rates[0].high + (2* (Pivot - Rates[0].low));
      S2 = Pivot - (R1 - S1);
      S3 = Rates[0].low - (2*(Rates[0].high - Pivot));
      
      PivotBuffer[i] = Pivot;
      R1Buffer[i] = R1;
      S1Buffer[i] = S1;
      R2Buffer[i] = R2;
      S2Buffer[i] = S2;
      R3Buffer[i] = R3;
      S3Buffer[i] = S3;
   }
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
